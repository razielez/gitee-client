
# Project

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**fullName** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
**path** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**owner** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**_private** | **String** |  |  [optional]
**fork** | **String** |  |  [optional]
**htmlUrl** | **String** |  |  [optional]
**forksUrl** | **String** |  |  [optional]
**keysUrl** | **String** |  |  [optional]
**collaboratorsUrl** | **String** |  |  [optional]
**hooksUrl** | **String** |  |  [optional]
**branchesUrl** | **String** |  |  [optional]
**tagsUrl** | **String** |  |  [optional]
**blobsUrl** | **String** |  |  [optional]
**stargazersUrl** | **String** |  |  [optional]
**contributorsUrl** | **String** |  |  [optional]
**commitsUrl** | **String** |  |  [optional]
**commentsUrl** | **String** |  |  [optional]
**issueCommentUrl** | **String** |  |  [optional]
**issuesUrl** | **String** |  |  [optional]
**pullsUrl** | **String** |  |  [optional]
**milestonesUrl** | **String** |  |  [optional]
**notificationsUrl** | **String** |  |  [optional]
**labelsUrl** | **String** |  |  [optional]
**releasesUrl** | **String** |  |  [optional]
**recommend** | **String** |  |  [optional]
**homepage** | **String** |  |  [optional]
**language** | **String** |  |  [optional]
**forksCount** | **String** |  |  [optional]
**stargazersCount** | **String** |  |  [optional]
**watchersCount** | **String** |  |  [optional]
**defaultBranch** | **String** |  |  [optional]
**openIssuesCount** | **Integer** |  |  [optional]
**hasIssues** | **String** |  |  [optional]
**hasWiki** | **String** |  |  [optional]
**pullRequestsEnabled** | **String** |  |  [optional]
**hasPage** | **String** |  |  [optional]
**license** | **String** |  |  [optional]
**pushedAt** | **String** |  |  [optional]
**createdAt** | **String** |  |  [optional]
**updatedAt** | **String** |  |  [optional]
**parent** | [**Project**](Project.md) |  |  [optional]
**paas** | **String** |  |  [optional]
**stared** | **String** |  |  [optional]
**watched** | **String** |  |  [optional]
**permission** | **String** |  |  [optional]
**relation** | **String** |  |  [optional]



