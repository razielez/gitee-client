
# UserMessage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**sender** | **String** |  |  [optional]
**unread** | **String** |  |  [optional]
**content** | **String** |  |  [optional]
**updatedAt** | **String** |  |  [optional]
**url** | **String** |  |  [optional]



