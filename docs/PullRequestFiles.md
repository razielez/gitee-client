
# PullRequestFiles

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sha** | **String** |  |  [optional]
**filename** | **String** |  |  [optional]
**status** | **String** |  |  [optional]
**additions** | **String** |  |  [optional]
**deletions** | **String** |  |  [optional]
**blobUrl** | **String** |  |  [optional]
**rawUrl** | **String** |  |  [optional]
**patch** | **String** |  |  [optional]



