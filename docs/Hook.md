
# Hook

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
**createdAt** | **String** |  |  [optional]
**password** | **String** |  |  [optional]
**projectId** | **String** |  |  [optional]
**result** | **String** |  |  [optional]
**resultCode** | **String** |  |  [optional]
**pushEvents** | **String** |  |  [optional]
**tagPushEvents** | **String** |  |  [optional]
**issuesEvents** | **String** |  |  [optional]
**noteEvents** | **String** |  |  [optional]
**mergeRequestsEvents** | **String** |  |  [optional]



