# RepositoriesApi

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteV5ReposOwnerRepo**](RepositoriesApi.md#deleteV5ReposOwnerRepo) | **DELETE** v5/repos/{owner}/{repo} | 删除一个项目
[**deleteV5ReposOwnerRepoBranchesBranchProtection**](RepositoriesApi.md#deleteV5ReposOwnerRepoBranchesBranchProtection) | **DELETE** v5/repos/{owner}/{repo}/branches/{branch}/protection | 取消保护分支的设置
[**deleteV5ReposOwnerRepoCollaboratorsUsername**](RepositoriesApi.md#deleteV5ReposOwnerRepoCollaboratorsUsername) | **DELETE** v5/repos/{owner}/{repo}/collaborators/{username} | 移除项目成员
[**deleteV5ReposOwnerRepoCommentsId**](RepositoriesApi.md#deleteV5ReposOwnerRepoCommentsId) | **DELETE** v5/repos/{owner}/{repo}/comments/{id} | 删除Commit评论
[**deleteV5ReposOwnerRepoContentsPath**](RepositoriesApi.md#deleteV5ReposOwnerRepoContentsPath) | **DELETE** v5/repos/{owner}/{repo}/contents/{path} | 删除文件
[**deleteV5ReposOwnerRepoKeysId**](RepositoriesApi.md#deleteV5ReposOwnerRepoKeysId) | **DELETE** v5/repos/{owner}/{repo}/keys/{id} | 删除一个项目公钥
[**deleteV5ReposOwnerRepoReleasesId**](RepositoriesApi.md#deleteV5ReposOwnerRepoReleasesId) | **DELETE** v5/repos/{owner}/{repo}/releases/{id} | 删除项目Release
[**getV5OrgsOrgRepos**](RepositoriesApi.md#getV5OrgsOrgRepos) | **GET** v5/orgs/{org}/repos | 获取一个组织的项目
[**getV5ReposOwnerRepo**](RepositoriesApi.md#getV5ReposOwnerRepo) | **GET** v5/repos/{owner}/{repo} | 列出授权用户的某个项目
[**getV5ReposOwnerRepoBranches**](RepositoriesApi.md#getV5ReposOwnerRepoBranches) | **GET** v5/repos/{owner}/{repo}/branches | 获取所有分支
[**getV5ReposOwnerRepoBranchesBranch**](RepositoriesApi.md#getV5ReposOwnerRepoBranchesBranch) | **GET** v5/repos/{owner}/{repo}/branches/{branch} | 获取单个分支
[**getV5ReposOwnerRepoCollaborators**](RepositoriesApi.md#getV5ReposOwnerRepoCollaborators) | **GET** v5/repos/{owner}/{repo}/collaborators | 获取项目的所有成员
[**getV5ReposOwnerRepoCollaboratorsUsername**](RepositoriesApi.md#getV5ReposOwnerRepoCollaboratorsUsername) | **GET** v5/repos/{owner}/{repo}/collaborators/{username} | 判断用户是否为项目成员
[**getV5ReposOwnerRepoCollaboratorsUsernamePermission**](RepositoriesApi.md#getV5ReposOwnerRepoCollaboratorsUsernamePermission) | **GET** v5/repos/{owner}/{repo}/collaborators/{username}/permission | 查看项目成员的权限
[**getV5ReposOwnerRepoComments**](RepositoriesApi.md#getV5ReposOwnerRepoComments) | **GET** v5/repos/{owner}/{repo}/comments | 获取项目的Commit评论
[**getV5ReposOwnerRepoCommentsId**](RepositoriesApi.md#getV5ReposOwnerRepoCommentsId) | **GET** v5/repos/{owner}/{repo}/comments/{id} | 获取项目的某条Commit评论
[**getV5ReposOwnerRepoCommits**](RepositoriesApi.md#getV5ReposOwnerRepoCommits) | **GET** v5/repos/{owner}/{repo}/commits | 项目的所有提交
[**getV5ReposOwnerRepoCommitsRefComments**](RepositoriesApi.md#getV5ReposOwnerRepoCommitsRefComments) | **GET** v5/repos/{owner}/{repo}/commits/{ref}/comments | 获取单个Commit的评论
[**getV5ReposOwnerRepoCommitsSha**](RepositoriesApi.md#getV5ReposOwnerRepoCommitsSha) | **GET** v5/repos/{owner}/{repo}/commits/{sha} | 项目的某个提交
[**getV5ReposOwnerRepoCompareBaseHead**](RepositoriesApi.md#getV5ReposOwnerRepoCompareBaseHead) | **GET** v5/repos/{owner}/{repo}/compare/{base}...{head} | 两个Commits之间对比的版本差异
[**getV5ReposOwnerRepoContentsPath**](RepositoriesApi.md#getV5ReposOwnerRepoContentsPath) | **GET** v5/repos/{owner}/{repo}/contents(/{path}) | 获取仓库具体路径下的内容
[**getV5ReposOwnerRepoContributors**](RepositoriesApi.md#getV5ReposOwnerRepoContributors) | **GET** v5/repos/{owner}/{repo}/contributors | 获取项目贡献者
[**getV5ReposOwnerRepoForks**](RepositoriesApi.md#getV5ReposOwnerRepoForks) | **GET** v5/repos/{owner}/{repo}/forks | 查看项目的Forks
[**getV5ReposOwnerRepoKeys**](RepositoriesApi.md#getV5ReposOwnerRepoKeys) | **GET** v5/repos/{owner}/{repo}/keys | 展示项目的公钥
[**getV5ReposOwnerRepoKeysId**](RepositoriesApi.md#getV5ReposOwnerRepoKeysId) | **GET** v5/repos/{owner}/{repo}/keys/{id} | 获取项目的单个公钥
[**getV5ReposOwnerRepoPages**](RepositoriesApi.md#getV5ReposOwnerRepoPages) | **GET** v5/repos/{owner}/{repo}/pages | 获取Pages信息
[**getV5ReposOwnerRepoReadme**](RepositoriesApi.md#getV5ReposOwnerRepoReadme) | **GET** v5/repos/{owner}/{repo}/readme | 获取仓库README
[**getV5ReposOwnerRepoReleases**](RepositoriesApi.md#getV5ReposOwnerRepoReleases) | **GET** v5/repos/{owner}/{repo}/releases | 获取项目的所有Releases
[**getV5ReposOwnerRepoReleasesId**](RepositoriesApi.md#getV5ReposOwnerRepoReleasesId) | **GET** v5/repos/{owner}/{repo}/releases/{id} | 获取项目的单个Releases
[**getV5ReposOwnerRepoReleasesLatest**](RepositoriesApi.md#getV5ReposOwnerRepoReleasesLatest) | **GET** v5/repos/{owner}/{repo}/releases/latest | 获取项目的最后更新的Release
[**getV5ReposOwnerRepoReleasesTagsTag**](RepositoriesApi.md#getV5ReposOwnerRepoReleasesTagsTag) | **GET** v5/repos/{owner}/{repo}/releases/tags/{tag} | 根据Tag名称获取项目的Release
[**getV5ReposOwnerRepoTags**](RepositoriesApi.md#getV5ReposOwnerRepoTags) | **GET** v5/repos/{owner}/{repo}/tags | 列出项目所有的tags
[**getV5UserRepos**](RepositoriesApi.md#getV5UserRepos) | **GET** v5/user/repos | 列出授权用户的所有项目
[**getV5UsersUsernameRepos**](RepositoriesApi.md#getV5UsersUsernameRepos) | **GET** v5/users/{username}/repos | 获取某个用户的公开项目
[**patchV5ReposOwnerRepo**](RepositoriesApi.md#patchV5ReposOwnerRepo) | **PATCH** v5/repos/{owner}/{repo} | 更新项目设置
[**patchV5ReposOwnerRepoCommentsId**](RepositoriesApi.md#patchV5ReposOwnerRepoCommentsId) | **PATCH** v5/repos/{owner}/{repo}/comments/{id} | 更新Commit评论
[**patchV5ReposOwnerRepoReleasesId**](RepositoriesApi.md#patchV5ReposOwnerRepoReleasesId) | **PATCH** v5/repos/{owner}/{repo}/releases/{id} | 更新项目Release
[**postV5OrgsOrgRepos**](RepositoriesApi.md#postV5OrgsOrgRepos) | **POST** v5/orgs/{org}/repos | 创建组织项目
[**postV5ReposOwnerRepoCommitsShaComments**](RepositoriesApi.md#postV5ReposOwnerRepoCommitsShaComments) | **POST** v5/repos/{owner}/{repo}/commits/{sha}/comments | 创建Commit评论
[**postV5ReposOwnerRepoContentsPath**](RepositoriesApi.md#postV5ReposOwnerRepoContentsPath) | **POST** v5/repos/{owner}/{repo}/contents/{path} | 新建文件
[**postV5ReposOwnerRepoForks**](RepositoriesApi.md#postV5ReposOwnerRepoForks) | **POST** v5/repos/{owner}/{repo}/forks | Fork一个项目
[**postV5ReposOwnerRepoKeys**](RepositoriesApi.md#postV5ReposOwnerRepoKeys) | **POST** v5/repos/{owner}/{repo}/keys | 为项目添加公钥
[**postV5ReposOwnerRepoPagesBuilds**](RepositoriesApi.md#postV5ReposOwnerRepoPagesBuilds) | **POST** v5/repos/{owner}/{repo}/pages/builds | 请求建立Pages
[**postV5ReposOwnerRepoReleases**](RepositoriesApi.md#postV5ReposOwnerRepoReleases) | **POST** v5/repos/{owner}/{repo}/releases | 创建项目Release
[**postV5UserRepos**](RepositoriesApi.md#postV5UserRepos) | **POST** v5/user/repos | 创建一个项目
[**putV5ReposOwnerRepoBranchesBranchProtection**](RepositoriesApi.md#putV5ReposOwnerRepoBranchesBranchProtection) | **PUT** v5/repos/{owner}/{repo}/branches/{branch}/protection | 设置分支保护
[**putV5ReposOwnerRepoCollaboratorsUsername**](RepositoriesApi.md#putV5ReposOwnerRepoCollaboratorsUsername) | **PUT** v5/repos/{owner}/{repo}/collaborators/{username} | 添加项目成员
[**putV5ReposOwnerRepoContentsPath**](RepositoriesApi.md#putV5ReposOwnerRepoContentsPath) | **PUT** v5/repos/{owner}/{repo}/contents/{path} | 更新文件


<a name="deleteV5ReposOwnerRepo"></a>
# **deleteV5ReposOwnerRepo**
> Void deleteV5ReposOwnerRepo(owner, repo, accessToken)

删除一个项目

删除一个项目

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.deleteV5ReposOwnerRepo(owner, repo, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteV5ReposOwnerRepoBranchesBranchProtection"></a>
# **deleteV5ReposOwnerRepoBranchesBranchProtection**
> Void deleteV5ReposOwnerRepoBranchesBranchProtection(owner, repo, branch, accessToken)

取消保护分支的设置

取消保护分支的设置

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String branch = "branch_example"; // String | 分支名称
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.deleteV5ReposOwnerRepoBranchesBranchProtection(owner, repo, branch, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **branch** | **String**| 分支名称 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteV5ReposOwnerRepoCollaboratorsUsername"></a>
# **deleteV5ReposOwnerRepoCollaboratorsUsername**
> Void deleteV5ReposOwnerRepoCollaboratorsUsername(owner, repo, username, accessToken)

移除项目成员

移除项目成员

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.deleteV5ReposOwnerRepoCollaboratorsUsername(owner, repo, username, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteV5ReposOwnerRepoCommentsId"></a>
# **deleteV5ReposOwnerRepoCommentsId**
> Void deleteV5ReposOwnerRepoCommentsId(owner, repo, id, accessToken)

删除Commit评论

删除Commit评论

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
Integer id = 56; // Integer | 评论的ID
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.deleteV5ReposOwnerRepoCommentsId(owner, repo, id, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **id** | **Integer**| 评论的ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteV5ReposOwnerRepoContentsPath"></a>
# **deleteV5ReposOwnerRepoContentsPath**
> CommitContent deleteV5ReposOwnerRepoContentsPath(owner, repo, path, sha, message, accessToken, branch, committerName, committerEmail, authorName, authorEmail)

删除文件

删除文件

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String path = "path_example"; // String | 文件的路径
String sha = "sha_example"; // String | 文件被替换的sha值
String message = "message_example"; // String | 提交信息
String accessToken = "accessToken_example"; // String | 用户授权码
String branch = "branch_example"; // String | 分支名称。默认为项目对默认分支
String committerName = "committerName_example"; // String | Committer的名字，默认为当前用户的名字
String committerEmail = "committerEmail_example"; // String | Committer的邮箱，默认为当前用户的邮箱
String authorName = "authorName_example"; // String | Author的名字，默认为当前用户的名字
String authorEmail = "authorEmail_example"; // String | Author的邮箱，默认为当前用户的邮箱
Observable<CommitContent> result = apiInstance.deleteV5ReposOwnerRepoContentsPath(owner, repo, path, sha, message, accessToken, branch, committerName, committerEmail, authorName, authorEmail);
result.subscribe(new Observer<CommitContent>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(CommitContent response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **path** | **String**| 文件的路径 |
 **sha** | **String**| 文件被替换的sha值 |
 **message** | **String**| 提交信息 |
 **accessToken** | **String**| 用户授权码 | [optional]
 **branch** | **String**| 分支名称。默认为项目对默认分支 | [optional]
 **committerName** | **String**| Committer的名字，默认为当前用户的名字 | [optional]
 **committerEmail** | **String**| Committer的邮箱，默认为当前用户的邮箱 | [optional]
 **authorName** | **String**| Author的名字，默认为当前用户的名字 | [optional]
 **authorEmail** | **String**| Author的邮箱，默认为当前用户的邮箱 | [optional]

### Return type

[**CommitContent**](CommitContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteV5ReposOwnerRepoKeysId"></a>
# **deleteV5ReposOwnerRepoKeysId**
> Void deleteV5ReposOwnerRepoKeysId(owner, repo, id, accessToken)

删除一个项目公钥

删除一个项目公钥

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
Integer id = 56; // Integer | 公钥 ID
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.deleteV5ReposOwnerRepoKeysId(owner, repo, id, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **id** | **Integer**| 公钥 ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteV5ReposOwnerRepoReleasesId"></a>
# **deleteV5ReposOwnerRepoReleasesId**
> Void deleteV5ReposOwnerRepoReleasesId(owner, repo, id, accessToken)

删除项目Release

删除项目Release

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
Integer id = 56; // Integer | 
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.deleteV5ReposOwnerRepoReleasesId(owner, repo, id, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **id** | **Integer**|  |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5OrgsOrgRepos"></a>
# **getV5OrgsOrgRepos**
> Project getV5OrgsOrgRepos(org, accessToken, type, page, perPage)

获取一个组织的项目

获取一个组织的项目

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String org = "org_example"; // String | 组织的路径(path/login)
String accessToken = "accessToken_example"; // String | 用户授权码
String type = "all"; // String | 筛选项目的类型，可以是 all, public, private。默认: all
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<Project> result = apiInstance.getV5OrgsOrgRepos(org, accessToken, type, page, perPage);
result.subscribe(new Observer<Project>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Project response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org** | **String**| 组织的路径(path/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **type** | **String**| 筛选项目的类型，可以是 all, public, private。默认: all | [optional] [default to all] [enum: all, public, private]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**Project**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepo"></a>
# **getV5ReposOwnerRepo**
> Void getV5ReposOwnerRepo(owner, repo, accessToken)

列出授权用户的某个项目

列出授权用户的某个项目

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.getV5ReposOwnerRepo(owner, repo, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoBranches"></a>
# **getV5ReposOwnerRepoBranches**
> java.util.List&lt;Branch&gt; getV5ReposOwnerRepoBranches(owner, repo, accessToken)

获取所有分支

获取所有分支

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<java.util.List<Branch>> result = apiInstance.getV5ReposOwnerRepoBranches(owner, repo, accessToken);
result.subscribe(new Observer<java.util.List<Branch>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<Branch> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**java.util.List&lt;Branch&gt;**](Branch.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoBranchesBranch"></a>
# **getV5ReposOwnerRepoBranchesBranch**
> CompleteBranch getV5ReposOwnerRepoBranchesBranch(owner, repo, branch, accessToken)

获取单个分支

获取单个分支

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String branch = "branch_example"; // String | 分支名称
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<CompleteBranch> result = apiInstance.getV5ReposOwnerRepoBranchesBranch(owner, repo, branch, accessToken);
result.subscribe(new Observer<CompleteBranch>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(CompleteBranch response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **branch** | **String**| 分支名称 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**CompleteBranch**](CompleteBranch.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoCollaborators"></a>
# **getV5ReposOwnerRepoCollaborators**
> Void getV5ReposOwnerRepoCollaborators(owner, repo, accessToken)

获取项目的所有成员

获取项目的所有成员

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.getV5ReposOwnerRepoCollaborators(owner, repo, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoCollaboratorsUsername"></a>
# **getV5ReposOwnerRepoCollaboratorsUsername**
> Void getV5ReposOwnerRepoCollaboratorsUsername(owner, repo, username, accessToken)

判断用户是否为项目成员

判断用户是否为项目成员

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.getV5ReposOwnerRepoCollaboratorsUsername(owner, repo, username, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoCollaboratorsUsernamePermission"></a>
# **getV5ReposOwnerRepoCollaboratorsUsernamePermission**
> Void getV5ReposOwnerRepoCollaboratorsUsernamePermission(owner, repo, username, accessToken)

查看项目成员的权限

查看项目成员的权限

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.getV5ReposOwnerRepoCollaboratorsUsernamePermission(owner, repo, username, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoComments"></a>
# **getV5ReposOwnerRepoComments**
> Void getV5ReposOwnerRepoComments(owner, repo, accessToken, page, perPage)

获取项目的Commit评论

获取项目的Commit评论

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<Void> result = apiInstance.getV5ReposOwnerRepoComments(owner, repo, accessToken, page, perPage);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoCommentsId"></a>
# **getV5ReposOwnerRepoCommentsId**
> Void getV5ReposOwnerRepoCommentsId(owner, repo, id, accessToken)

获取项目的某条Commit评论

获取项目的某条Commit评论

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
Integer id = 56; // Integer | 评论的ID
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.getV5ReposOwnerRepoCommentsId(owner, repo, id, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **id** | **Integer**| 评论的ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoCommits"></a>
# **getV5ReposOwnerRepoCommits**
> java.util.List&lt;RepoCommit&gt; getV5ReposOwnerRepoCommits(owner, repo, accessToken, sha, path, author, since, until, page, perPage)

项目的所有提交

项目的所有提交

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
String sha = "sha_example"; // String | 提交起始的SHA值或者分支名. 默认: 项目的默认分支
String path = "path_example"; // String | 包含该文件的提交
String author = "author_example"; // String | 提交作者的邮箱或个性地址(username/login)
String since = "since_example"; // String | 提交的起始时间，时间格式为 ISO 8601
String until = "until_example"; // String | 提交的最后时间，时间格式为 ISO 8601
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<java.util.List<RepoCommit>> result = apiInstance.getV5ReposOwnerRepoCommits(owner, repo, accessToken, sha, path, author, since, until, page, perPage);
result.subscribe(new Observer<java.util.List<RepoCommit>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<RepoCommit> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **sha** | **String**| 提交起始的SHA值或者分支名. 默认: 项目的默认分支 | [optional]
 **path** | **String**| 包含该文件的提交 | [optional]
 **author** | **String**| 提交作者的邮箱或个性地址(username/login) | [optional]
 **since** | **String**| 提交的起始时间，时间格式为 ISO 8601 | [optional]
 **until** | **String**| 提交的最后时间，时间格式为 ISO 8601 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**java.util.List&lt;RepoCommit&gt;**](RepoCommit.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoCommitsRefComments"></a>
# **getV5ReposOwnerRepoCommitsRefComments**
> Void getV5ReposOwnerRepoCommitsRefComments(owner, repo, ref, accessToken, page, perPage)

获取单个Commit的评论

获取单个Commit的评论

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String ref = "ref_example"; // String | Commit的Reference
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<Void> result = apiInstance.getV5ReposOwnerRepoCommitsRefComments(owner, repo, ref, accessToken, page, perPage);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **ref** | **String**| Commit的Reference |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoCommitsSha"></a>
# **getV5ReposOwnerRepoCommitsSha**
> RepoCommit getV5ReposOwnerRepoCommitsSha(owner, repo, sha, accessToken)

项目的某个提交

项目的某个提交

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String sha = "sha_example"; // String | 提交的SHA值或者分支名
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<RepoCommit> result = apiInstance.getV5ReposOwnerRepoCommitsSha(owner, repo, sha, accessToken);
result.subscribe(new Observer<RepoCommit>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(RepoCommit response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **sha** | **String**| 提交的SHA值或者分支名 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**RepoCommit**](RepoCommit.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoCompareBaseHead"></a>
# **getV5ReposOwnerRepoCompareBaseHead**
> Compare getV5ReposOwnerRepoCompareBaseHead(owner, repo, base, head, accessToken)

两个Commits之间对比的版本差异

两个Commits之间对比的版本差异

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String base = "base_example"; // String | Commit提交的SHA值或者分支名作为对比起点
String head = "head_example"; // String | Commit提交的SHA值或者分支名作为对比终点
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Compare> result = apiInstance.getV5ReposOwnerRepoCompareBaseHead(owner, repo, base, head, accessToken);
result.subscribe(new Observer<Compare>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Compare response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **base** | **String**| Commit提交的SHA值或者分支名作为对比起点 |
 **head** | **String**| Commit提交的SHA值或者分支名作为对比终点 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Compare**](Compare.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoContentsPath"></a>
# **getV5ReposOwnerRepoContentsPath**
> java.util.List&lt;Content&gt; getV5ReposOwnerRepoContentsPath(owner, repo, path, accessToken, ref)

获取仓库具体路径下的内容

获取仓库具体路径下的内容

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String path = "path_example"; // String | 文件的路径
String accessToken = "accessToken_example"; // String | 用户授权码
String ref = "ref_example"; // String | 分支、tag或commit。默认: 项目的默认分支(通常是master)
Observable<java.util.List<Content>> result = apiInstance.getV5ReposOwnerRepoContentsPath(owner, repo, path, accessToken, ref);
result.subscribe(new Observer<java.util.List<Content>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<Content> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **path** | **String**| 文件的路径 |
 **accessToken** | **String**| 用户授权码 | [optional]
 **ref** | **String**| 分支、tag或commit。默认: 项目的默认分支(通常是master) | [optional]

### Return type

[**java.util.List&lt;Content&gt;**](Content.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoContributors"></a>
# **getV5ReposOwnerRepoContributors**
> Void getV5ReposOwnerRepoContributors(owner, repo, accessToken)

获取项目贡献者

获取项目贡献者

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.getV5ReposOwnerRepoContributors(owner, repo, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoForks"></a>
# **getV5ReposOwnerRepoForks**
> Void getV5ReposOwnerRepoForks(owner, repo, accessToken, sort, page, perPage)

查看项目的Forks

查看项目的Forks

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
String sort = "newest"; // String | 排序方式: fork的时间(newest, oldest)，star的人数(stargazers)
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<Void> result = apiInstance.getV5ReposOwnerRepoForks(owner, repo, accessToken, sort, page, perPage);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **sort** | **String**| 排序方式: fork的时间(newest, oldest)，star的人数(stargazers) | [optional] [default to newest] [enum: newest, oldest, stargazers]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoKeys"></a>
# **getV5ReposOwnerRepoKeys**
> java.util.List&lt;SSHKey&gt; getV5ReposOwnerRepoKeys(owner, repo, accessToken, page, perPage)

展示项目的公钥

展示项目的公钥

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<java.util.List<SSHKey>> result = apiInstance.getV5ReposOwnerRepoKeys(owner, repo, accessToken, page, perPage);
result.subscribe(new Observer<java.util.List<SSHKey>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<SSHKey> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**java.util.List&lt;SSHKey&gt;**](SSHKey.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoKeysId"></a>
# **getV5ReposOwnerRepoKeysId**
> SSHKey getV5ReposOwnerRepoKeysId(owner, repo, id, accessToken)

获取项目的单个公钥

获取项目的单个公钥

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
Integer id = 56; // Integer | 公钥 ID
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<SSHKey> result = apiInstance.getV5ReposOwnerRepoKeysId(owner, repo, id, accessToken);
result.subscribe(new Observer<SSHKey>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(SSHKey response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **id** | **Integer**| 公钥 ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**SSHKey**](SSHKey.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoPages"></a>
# **getV5ReposOwnerRepoPages**
> Void getV5ReposOwnerRepoPages(owner, repo, accessToken)

获取Pages信息

获取Pages信息

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.getV5ReposOwnerRepoPages(owner, repo, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoReadme"></a>
# **getV5ReposOwnerRepoReadme**
> Content getV5ReposOwnerRepoReadme(owner, repo, accessToken, ref)

获取仓库README

获取仓库README

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
String ref = "ref_example"; // String | 分支、tag或commit。默认: 项目的默认分支(通常是master)
Observable<Content> result = apiInstance.getV5ReposOwnerRepoReadme(owner, repo, accessToken, ref);
result.subscribe(new Observer<Content>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Content response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **ref** | **String**| 分支、tag或commit。默认: 项目的默认分支(通常是master) | [optional]

### Return type

[**Content**](Content.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoReleases"></a>
# **getV5ReposOwnerRepoReleases**
> java.util.List&lt;Release&gt; getV5ReposOwnerRepoReleases(owner, repo, accessToken, page, perPage)

获取项目的所有Releases

获取项目的所有Releases

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<java.util.List<Release>> result = apiInstance.getV5ReposOwnerRepoReleases(owner, repo, accessToken, page, perPage);
result.subscribe(new Observer<java.util.List<Release>>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(java.util.List<Release> response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**java.util.List&lt;Release&gt;**](Release.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoReleasesId"></a>
# **getV5ReposOwnerRepoReleasesId**
> Release getV5ReposOwnerRepoReleasesId(owner, repo, id, accessToken)

获取项目的单个Releases

获取项目的单个Releases

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
Integer id = 56; // Integer | 发行版本的ID
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Release> result = apiInstance.getV5ReposOwnerRepoReleasesId(owner, repo, id, accessToken);
result.subscribe(new Observer<Release>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Release response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **id** | **Integer**| 发行版本的ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Release**](Release.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoReleasesLatest"></a>
# **getV5ReposOwnerRepoReleasesLatest**
> Release getV5ReposOwnerRepoReleasesLatest(owner, repo, accessToken)

获取项目的最后更新的Release

获取项目的最后更新的Release

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Release> result = apiInstance.getV5ReposOwnerRepoReleasesLatest(owner, repo, accessToken);
result.subscribe(new Observer<Release>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Release response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Release**](Release.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoReleasesTagsTag"></a>
# **getV5ReposOwnerRepoReleasesTagsTag**
> Release getV5ReposOwnerRepoReleasesTagsTag(owner, repo, tag, accessToken)

根据Tag名称获取项目的Release

根据Tag名称获取项目的Release

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String tag = "tag_example"; // String | Tag 名称
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Release> result = apiInstance.getV5ReposOwnerRepoReleasesTagsTag(owner, repo, tag, accessToken);
result.subscribe(new Observer<Release>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Release response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **tag** | **String**| Tag 名称 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Release**](Release.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoTags"></a>
# **getV5ReposOwnerRepoTags**
> Void getV5ReposOwnerRepoTags(owner, repo, accessToken)

列出项目所有的tags

列出项目所有的tags

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.getV5ReposOwnerRepoTags(owner, repo, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5UserRepos"></a>
# **getV5UserRepos**
> Void getV5UserRepos(accessToken, visibility, affiliation, type, sort, direction, page, perPage)

列出授权用户的所有项目

列出授权用户的所有项目

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String accessToken = "accessToken_example"; // String | 用户授权码
String visibility = "visibility_example"; // String | 公开(public)、私有(private)或者所有(all)，默认: 所有(all)
String affiliation = "affiliation_example"; // String | owner(授权用户拥有的项目)、collaborator(授权用户为项目成员)、organization_member(授权用户为项目所在组织并有访问项目权限)。                    可以用逗号分隔符组合。如: owner, organization_member 或 owner, collaborator, organization_member
String type = "type_example"; // String | 不能与visibility或affiliation参数一并使用，否则会报422错误
String sort = "full_name"; // String | 排序方式: 创建时间(created)，更新时间(updated)，最后推送时间(pushed)，项目所属与名称(full_name)。默认: full_name
String direction = "direction_example"; // String | 如果sort参数为full_name，用升序(asc)。否则降序(desc)
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<Void> result = apiInstance.getV5UserRepos(accessToken, visibility, affiliation, type, sort, direction, page, perPage);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]
 **visibility** | **String**| 公开(public)、私有(private)或者所有(all)，默认: 所有(all) | [optional] [enum: private, public, all]
 **affiliation** | **String**| owner(授权用户拥有的项目)、collaborator(授权用户为项目成员)、organization_member(授权用户为项目所在组织并有访问项目权限)。                    可以用逗号分隔符组合。如: owner, organization_member 或 owner, collaborator, organization_member | [optional]
 **type** | **String**| 不能与visibility或affiliation参数一并使用，否则会报422错误 | [optional] [enum: all, owner, public, private, member]
 **sort** | **String**| 排序方式: 创建时间(created)，更新时间(updated)，最后推送时间(pushed)，项目所属与名称(full_name)。默认: full_name | [optional] [default to full_name] [enum: created, updated, pushed, full_name]
 **direction** | **String**| 如果sort参数为full_name，用升序(asc)。否则降序(desc) | [optional] [enum: asc, desc]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5UsersUsernameRepos"></a>
# **getV5UsersUsernameRepos**
> Void getV5UsersUsernameRepos(username, accessToken, type, sort, direction, page, perPage)

获取某个用户的公开项目

获取某个用户的公开项目

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
String type = "all"; // String | 用户创建的项目(owner)，用户为项目成员(member)，所有(all)。默认: 所有(all)
String sort = "full_name"; // String | 排序方式: 创建时间(created)，更新时间(updated)，最后推送时间(pushed)，项目所属与名称(full_name)。默认: full_name
String direction = "direction_example"; // String | 如果sort参数为full_name，用升序(asc)。否则降序(desc)
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量
Observable<Void> result = apiInstance.getV5UsersUsernameRepos(username, accessToken, type, sort, direction, page, perPage);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **type** | **String**| 用户创建的项目(owner)，用户为项目成员(member)，所有(all)。默认: 所有(all) | [optional] [default to all] [enum: all, owner, member]
 **sort** | **String**| 排序方式: 创建时间(created)，更新时间(updated)，最后推送时间(pushed)，项目所属与名称(full_name)。默认: full_name | [optional] [default to full_name] [enum: created, updated, pushed, full_name]
 **direction** | **String**| 如果sort参数为full_name，用升序(asc)。否则降序(desc) | [optional] [enum: asc, desc]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量 | [optional] [default to 20]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="patchV5ReposOwnerRepo"></a>
# **patchV5ReposOwnerRepo**
> Void patchV5ReposOwnerRepo(owner, repo, name, accessToken, description, homepage, hasIssues, hasWiki, _private, defaultBranch)

更新项目设置

更新项目设置

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String name = "name_example"; // String | 项目名称
String accessToken = "accessToken_example"; // String | 用户授权码
String description = "description_example"; // String | 项目描述
String homepage = "homepage_example"; // String | 项目所在地址
Boolean hasIssues = true; // Boolean | 允许提Issue与否。默认: 允许(true)
Boolean hasWiki = true; // Boolean | 提供Wiki与否。默认: 提供(true)
Boolean _private = true; // Boolean | 项目公开或私有。
String defaultBranch = "defaultBranch_example"; // String | 更新默认分支
Observable<Void> result = apiInstance.patchV5ReposOwnerRepo(owner, repo, name, accessToken, description, homepage, hasIssues, hasWiki, _private, defaultBranch);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **name** | **String**| 项目名称 |
 **accessToken** | **String**| 用户授权码 | [optional]
 **description** | **String**| 项目描述 | [optional]
 **homepage** | **String**| 项目所在地址 | [optional]
 **hasIssues** | **Boolean**| 允许提Issue与否。默认: 允许(true) | [optional] [default to true]
 **hasWiki** | **Boolean**| 提供Wiki与否。默认: 提供(true) | [optional] [default to true]
 **_private** | **Boolean**| 项目公开或私有。 | [optional]
 **defaultBranch** | **String**| 更新默认分支 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="patchV5ReposOwnerRepoCommentsId"></a>
# **patchV5ReposOwnerRepoCommentsId**
> Void patchV5ReposOwnerRepoCommentsId(owner, repo, id, body, accessToken)

更新Commit评论

更新Commit评论

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
Integer id = 56; // Integer | 评论的ID
String body = "body_example"; // String | 评论的内容
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.patchV5ReposOwnerRepoCommentsId(owner, repo, id, body, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **id** | **Integer**| 评论的ID |
 **body** | **String**| 评论的内容 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="patchV5ReposOwnerRepoReleasesId"></a>
# **patchV5ReposOwnerRepoReleasesId**
> Release patchV5ReposOwnerRepoReleasesId(owner, repo, tagName, name, body, id, accessToken, prerelease)

更新项目Release

更新项目Release

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String tagName = "tagName_example"; // String | Tag 名称, 提倡以v字母为前缀做为Release名称，例如v1.0或者v2.3.4
String name = "name_example"; // String | Release 名称
String body = "body_example"; // String | Release 描述
Integer id = 56; // Integer | 
String accessToken = "accessToken_example"; // String | 用户授权码
Boolean prerelease = true; // Boolean | 是否为预览版本。默认: false（非预览版本）
Observable<Release> result = apiInstance.patchV5ReposOwnerRepoReleasesId(owner, repo, tagName, name, body, id, accessToken, prerelease);
result.subscribe(new Observer<Release>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Release response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **tagName** | **String**| Tag 名称, 提倡以v字母为前缀做为Release名称，例如v1.0或者v2.3.4 |
 **name** | **String**| Release 名称 |
 **body** | **String**| Release 描述 |
 **id** | **Integer**|  |
 **accessToken** | **String**| 用户授权码 | [optional]
 **prerelease** | **Boolean**| 是否为预览版本。默认: false（非预览版本） | [optional]

### Return type

[**Release**](Release.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postV5OrgsOrgRepos"></a>
# **postV5OrgsOrgRepos**
> Void postV5OrgsOrgRepos(name, org, accessToken, description, homepage, hasIssues, hasWiki, _private, autoInit, gitignoreTemplate, licenseTemplate)

创建组织项目

创建组织项目

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String name = "name_example"; // String | 项目名称
Integer org = 56; // Integer | 
String accessToken = "accessToken_example"; // String | 用户授权码
String description = "description_example"; // String | 项目描述
String homepage = "homepage_example"; // String | 项目所在地址
Boolean hasIssues = true; // Boolean | 允许提Issue与否。默认: 允许(true)
Boolean hasWiki = true; // Boolean | 提供Wiki与否。默认: 提供(true)
Boolean _private = true; // Boolean | 项目公开或私有。默认: 公开(false)
Boolean autoInit = true; // Boolean | 值为true时则会用README初始化仓库。默认: 不初始化(false)
String gitignoreTemplate = "gitignoreTemplate_example"; // String | Git Ingore模版
String licenseTemplate = "licenseTemplate_example"; // String | Git Ingore模版
Observable<Void> result = apiInstance.postV5OrgsOrgRepos(name, org, accessToken, description, homepage, hasIssues, hasWiki, _private, autoInit, gitignoreTemplate, licenseTemplate);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**| 项目名称 |
 **org** | **Integer**|  |
 **accessToken** | **String**| 用户授权码 | [optional]
 **description** | **String**| 项目描述 | [optional]
 **homepage** | **String**| 项目所在地址 | [optional]
 **hasIssues** | **Boolean**| 允许提Issue与否。默认: 允许(true) | [optional] [default to true]
 **hasWiki** | **Boolean**| 提供Wiki与否。默认: 提供(true) | [optional] [default to true]
 **_private** | **Boolean**| 项目公开或私有。默认: 公开(false) | [optional]
 **autoInit** | **Boolean**| 值为true时则会用README初始化仓库。默认: 不初始化(false) | [optional]
 **gitignoreTemplate** | **String**| Git Ingore模版 | [optional] [enum: Actionscript, Android, AppceleratorTitanium, Autotools, C, C++, CakePHP, CFWheels, Clojure, CMake, CodeIgniter, Composer, Concrete5, Coq, D, Dart, Delphi, Drupal, Eagle, Elixir, Erlang, ExpressionEngine, ExtJs, Finale, ForceDotCom, FuelPHP, Gcov, Go, Gradle, Grails, GWT, Haskell, Java, Jboss, Jekyll, Joomla, Kohana, Kotlin, Laravel, Leiningen, LemonStand, Lilypond, Lithium, Lua, Magento, Maven, Nanoc, Node, Objective-C, OCaml, Opa, Opencart, OracleForms, Perl, PlayFramework, Plone, Python, Qooxdoo, Qt, R, ROS, Rails, RhodesRhomobile, Ruby, Rust, Sass, Scala, Sdcc, SeamGen, SketchUp, SugarCRM, Swift, Symfony, SymphonyCMS, Textpattern, TurboGears2, Typo3, Unity, VisualStudio, Waf, WordPress, Yii, ZendFramework, Global/Archives, Global/Ansible, Global/CVS, Global/Eclipse, Global/Emacs, Global/Espresso, Global/FlexBuilder, Global/JetBrains, Global/Linux, Global/macOS, Global/Matlab, Global/Mercurial, Global/ModelSim, Global/MonoDevelop, Global/NetBeans, Global/Redcar, Global/Redis, Global/SBT, Global/SublimeText, Global/SVN, Global/Tags, Global/TextMate, Global/Vim, Global/VirtualEnv, Global/Windows, Global/Xcode, Global/XilinxISE]
 **licenseTemplate** | **String**| Git Ingore模版 | [optional] [enum: Apache v2 License, MIT License, GPL v2, Artistic License 2.0, BSD 2-Clause License, Affero GPL, LGPL v2.1, BSD (3-Clause) License, Eclipse Public License v1.0, LGPL v3, Mozilla Public License Version 2.0, GPL v3, WTFPL, Zlib]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postV5ReposOwnerRepoCommitsShaComments"></a>
# **postV5ReposOwnerRepoCommitsShaComments**
> Void postV5ReposOwnerRepoCommitsShaComments(owner, repo, sha, body, accessToken, path, position)

创建Commit评论

创建Commit评论

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String sha = "sha_example"; // String | 评论的sha值
String body = "body_example"; // String | 评论的内容
String accessToken = "accessToken_example"; // String | 用户授权码
String path = "path_example"; // String | 文件的相对路径
Integer position = 56; // Integer | Diff的相对行数
Observable<Void> result = apiInstance.postV5ReposOwnerRepoCommitsShaComments(owner, repo, sha, body, accessToken, path, position);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **sha** | **String**| 评论的sha值 |
 **body** | **String**| 评论的内容 |
 **accessToken** | **String**| 用户授权码 | [optional]
 **path** | **String**| 文件的相对路径 | [optional]
 **position** | **Integer**| Diff的相对行数 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postV5ReposOwnerRepoContentsPath"></a>
# **postV5ReposOwnerRepoContentsPath**
> CommitContent postV5ReposOwnerRepoContentsPath(owner, repo, path, content, message, accessToken, branch, committerName, committerEmail, authorName, authorEmail)

新建文件

新建文件

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String path = "path_example"; // String | 文件的路径
String content = "content_example"; // String | 文件内容, 要用base64编码
String message = "message_example"; // String | 提交信息
String accessToken = "accessToken_example"; // String | 用户授权码
String branch = "branch_example"; // String | 分支名称。默认为项目对默认分支
String committerName = "committerName_example"; // String | Committer的名字，默认为当前用户的名字
String committerEmail = "committerEmail_example"; // String | Committer的邮箱，默认为当前用户的邮箱
String authorName = "authorName_example"; // String | Author的名字，默认为当前用户的名字
String authorEmail = "authorEmail_example"; // String | Author的邮箱，默认为当前用户的邮箱
Observable<CommitContent> result = apiInstance.postV5ReposOwnerRepoContentsPath(owner, repo, path, content, message, accessToken, branch, committerName, committerEmail, authorName, authorEmail);
result.subscribe(new Observer<CommitContent>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(CommitContent response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **path** | **String**| 文件的路径 |
 **content** | **String**| 文件内容, 要用base64编码 |
 **message** | **String**| 提交信息 |
 **accessToken** | **String**| 用户授权码 | [optional]
 **branch** | **String**| 分支名称。默认为项目对默认分支 | [optional]
 **committerName** | **String**| Committer的名字，默认为当前用户的名字 | [optional]
 **committerEmail** | **String**| Committer的邮箱，默认为当前用户的邮箱 | [optional]
 **authorName** | **String**| Author的名字，默认为当前用户的名字 | [optional]
 **authorEmail** | **String**| Author的邮箱，默认为当前用户的邮箱 | [optional]

### Return type

[**CommitContent**](CommitContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postV5ReposOwnerRepoForks"></a>
# **postV5ReposOwnerRepoForks**
> Void postV5ReposOwnerRepoForks(owner, repo, accessToken, organization)

Fork一个项目

Fork一个项目

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
String organization = "organization_example"; // String | 组织地址，不填写默认Fork到用户个性地址
Observable<Void> result = apiInstance.postV5ReposOwnerRepoForks(owner, repo, accessToken, organization);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **organization** | **String**| 组织地址，不填写默认Fork到用户个性地址 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postV5ReposOwnerRepoKeys"></a>
# **postV5ReposOwnerRepoKeys**
> SSHKey postV5ReposOwnerRepoKeys(owner, repo, accessToken)

为项目添加公钥

为项目添加公钥

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<SSHKey> result = apiInstance.postV5ReposOwnerRepoKeys(owner, repo, accessToken);
result.subscribe(new Observer<SSHKey>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(SSHKey response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**SSHKey**](SSHKey.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postV5ReposOwnerRepoPagesBuilds"></a>
# **postV5ReposOwnerRepoPagesBuilds**
> Void postV5ReposOwnerRepoPagesBuilds(owner, repo, accessToken)

请求建立Pages

请求建立Pages

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.postV5ReposOwnerRepoPagesBuilds(owner, repo, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postV5ReposOwnerRepoReleases"></a>
# **postV5ReposOwnerRepoReleases**
> Release postV5ReposOwnerRepoReleases(owner, repo, tagName, name, body, targetCommitish, accessToken, prerelease)

创建项目Release

创建项目Release

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String tagName = "tagName_example"; // String | Tag 名称, 提倡以v字母为前缀做为Release名称，例如v1.0或者v2.3.4
String name = "name_example"; // String | Release 名称
String body = "body_example"; // String | Release 描述
String targetCommitish = "targetCommitish_example"; // String | 分支名称或者commit SHA, 默认是当前默认分支
String accessToken = "accessToken_example"; // String | 用户授权码
Boolean prerelease = true; // Boolean | 是否为预览版本。默认: false（非预览版本）
Observable<Release> result = apiInstance.postV5ReposOwnerRepoReleases(owner, repo, tagName, name, body, targetCommitish, accessToken, prerelease);
result.subscribe(new Observer<Release>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Release response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **tagName** | **String**| Tag 名称, 提倡以v字母为前缀做为Release名称，例如v1.0或者v2.3.4 |
 **name** | **String**| Release 名称 |
 **body** | **String**| Release 描述 |
 **targetCommitish** | **String**| 分支名称或者commit SHA, 默认是当前默认分支 |
 **accessToken** | **String**| 用户授权码 | [optional]
 **prerelease** | **Boolean**| 是否为预览版本。默认: false（非预览版本） | [optional]

### Return type

[**Release**](Release.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postV5UserRepos"></a>
# **postV5UserRepos**
> Void postV5UserRepos(name, accessToken, description, homepage, hasIssues, hasWiki, _private, autoInit, gitignoreTemplate, licenseTemplate)

创建一个项目

创建一个项目

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String name = "name_example"; // String | 项目名称
String accessToken = "accessToken_example"; // String | 用户授权码
String description = "description_example"; // String | 项目描述
String homepage = "homepage_example"; // String | 项目所在地址
Boolean hasIssues = true; // Boolean | 允许提Issue与否。默认: 允许(true)
Boolean hasWiki = true; // Boolean | 提供Wiki与否。默认: 提供(true)
Boolean _private = true; // Boolean | 项目公开或私有。默认: 公开(false)
Boolean autoInit = true; // Boolean | 值为true时则会用README初始化仓库。默认: 不初始化(false)
String gitignoreTemplate = "gitignoreTemplate_example"; // String | Git Ingore模版
String licenseTemplate = "licenseTemplate_example"; // String | License模版
Observable<Void> result = apiInstance.postV5UserRepos(name, accessToken, description, homepage, hasIssues, hasWiki, _private, autoInit, gitignoreTemplate, licenseTemplate);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**| 项目名称 |
 **accessToken** | **String**| 用户授权码 | [optional]
 **description** | **String**| 项目描述 | [optional]
 **homepage** | **String**| 项目所在地址 | [optional]
 **hasIssues** | **Boolean**| 允许提Issue与否。默认: 允许(true) | [optional] [default to true]
 **hasWiki** | **Boolean**| 提供Wiki与否。默认: 提供(true) | [optional] [default to true]
 **_private** | **Boolean**| 项目公开或私有。默认: 公开(false) | [optional]
 **autoInit** | **Boolean**| 值为true时则会用README初始化仓库。默认: 不初始化(false) | [optional]
 **gitignoreTemplate** | **String**| Git Ingore模版 | [optional] [enum: Actionscript, Android, AppceleratorTitanium, Autotools, C, C++, CakePHP, CFWheels, Clojure, CMake, CodeIgniter, Composer, Concrete5, Coq, D, Dart, Delphi, Drupal, Eagle, Elixir, Erlang, ExpressionEngine, ExtJs, Finale, ForceDotCom, FuelPHP, Gcov, Go, Gradle, Grails, GWT, Haskell, Java, Jboss, Jekyll, Joomla, Kohana, Kotlin, Laravel, Leiningen, LemonStand, Lilypond, Lithium, Lua, Magento, Maven, Nanoc, Node, Objective-C, OCaml, Opa, Opencart, OracleForms, Perl, PlayFramework, Plone, Python, Qooxdoo, Qt, R, ROS, Rails, RhodesRhomobile, Ruby, Rust, Sass, Scala, Sdcc, SeamGen, SketchUp, SugarCRM, Swift, Symfony, SymphonyCMS, Textpattern, TurboGears2, Typo3, Unity, VisualStudio, Waf, WordPress, Yii, ZendFramework, Global/Archives, Global/Ansible, Global/CVS, Global/Eclipse, Global/Emacs, Global/Espresso, Global/FlexBuilder, Global/JetBrains, Global/Linux, Global/macOS, Global/Matlab, Global/Mercurial, Global/ModelSim, Global/MonoDevelop, Global/NetBeans, Global/Redcar, Global/Redis, Global/SBT, Global/SublimeText, Global/SVN, Global/Tags, Global/TextMate, Global/Vim, Global/VirtualEnv, Global/Windows, Global/Xcode, Global/XilinxISE]
 **licenseTemplate** | **String**| License模版 | [optional] [enum: Apache v2 License, MIT License, GPL v2, Artistic License 2.0, BSD 2-Clause License, Affero GPL, LGPL v2.1, BSD (3-Clause) License, Eclipse Public License v1.0, LGPL v3, Mozilla Public License Version 2.0, GPL v3, WTFPL, Zlib]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="putV5ReposOwnerRepoBranchesBranchProtection"></a>
# **putV5ReposOwnerRepoBranchesBranchProtection**
> CompleteBranch putV5ReposOwnerRepoBranchesBranchProtection(owner, repo, branch, accessToken)

设置分支保护

设置分支保护

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String branch = "branch_example"; // String | 分支名称
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<CompleteBranch> result = apiInstance.putV5ReposOwnerRepoBranchesBranchProtection(owner, repo, branch, accessToken);
result.subscribe(new Observer<CompleteBranch>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(CompleteBranch response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **branch** | **String**| 分支名称 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**CompleteBranch**](CompleteBranch.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="putV5ReposOwnerRepoCollaboratorsUsername"></a>
# **putV5ReposOwnerRepoCollaboratorsUsername**
> Void putV5ReposOwnerRepoCollaboratorsUsername(owner, repo, username, permission, accessToken)

添加项目成员

添加项目成员

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String username = "username_example"; // String | 用户名(username/login)
String permission = "push"; // String | 成员权限: 拉代码(pull)，推代码(push)，管理员(admin)。默认: push
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.putV5ReposOwnerRepoCollaboratorsUsername(owner, repo, username, permission, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **username** | **String**| 用户名(username/login) |
 **permission** | **String**| 成员权限: 拉代码(pull)，推代码(push)，管理员(admin)。默认: push | [default to push] [enum: pull, push, admin]
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="putV5ReposOwnerRepoContentsPath"></a>
# **putV5ReposOwnerRepoContentsPath**
> CommitContent putV5ReposOwnerRepoContentsPath(owner, repo, path, content, sha, message, accessToken, branch, committerName, committerEmail, authorName, authorEmail)

更新文件

更新文件

### Example
```java
// Import classes:
//import com.gitee.api.api.RepositoriesApi;

RepositoriesApi apiInstance =  new ApiClient().create(RepositoriesApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String path = "path_example"; // String | 文件的路径
String content = "content_example"; // String | 文件内容, 要用base64编码
String sha = "sha_example"; // String | 文件被替换的sha值
String message = "message_example"; // String | 提交信息
String accessToken = "accessToken_example"; // String | 用户授权码
String branch = "branch_example"; // String | 分支名称。默认为项目对默认分支
String committerName = "committerName_example"; // String | Committer的名字，默认为当前用户的名字
String committerEmail = "committerEmail_example"; // String | Committer的邮箱，默认为当前用户的邮箱
String authorName = "authorName_example"; // String | Author的名字，默认为当前用户的名字
String authorEmail = "authorEmail_example"; // String | Author的邮箱，默认为当前用户的邮箱
Observable<CommitContent> result = apiInstance.putV5ReposOwnerRepoContentsPath(owner, repo, path, content, sha, message, accessToken, branch, committerName, committerEmail, authorName, authorEmail);
result.subscribe(new Observer<CommitContent>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(CommitContent response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **path** | **String**| 文件的路径 |
 **content** | **String**| 文件内容, 要用base64编码 |
 **sha** | **String**| 文件被替换的sha值 |
 **message** | **String**| 提交信息 |
 **accessToken** | **String**| 用户授权码 | [optional]
 **branch** | **String**| 分支名称。默认为项目对默认分支 | [optional]
 **committerName** | **String**| Committer的名字，默认为当前用户的名字 | [optional]
 **committerEmail** | **String**| Committer的邮箱，默认为当前用户的邮箱 | [optional]
 **authorName** | **String**| Author的名字，默认为当前用户的名字 | [optional]
 **authorEmail** | **String**| Author的邮箱，默认为当前用户的邮箱 | [optional]

### Return type

[**CommitContent**](CommitContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

