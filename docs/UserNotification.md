
# UserNotification

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**repository** | [**ProjectBasic**](ProjectBasic.md) |  |  [optional]
**unread** | **String** |  |  [optional]
**mute** | **String** |  |  [optional]
**subject** | **String** |  |  [optional]
**updatedAt** | **String** |  |  [optional]
**url** | **String** |  |  [optional]



