# MiscellaneousApi

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getV5Emojis**](MiscellaneousApi.md#getV5Emojis) | **GET** v5/emojis | 列出可使用的 Emoji
[**getV5GitignoreTemplates**](MiscellaneousApi.md#getV5GitignoreTemplates) | **GET** v5/gitignore/templates | 列出可使用的 .gitignore 模板
[**getV5GitignoreTemplatesName**](MiscellaneousApi.md#getV5GitignoreTemplatesName) | **GET** v5/gitignore/templates/{name} | 获取一个 .gitignore 模板
[**getV5GitignoreTemplatesNameRaw**](MiscellaneousApi.md#getV5GitignoreTemplatesNameRaw) | **GET** v5/gitignore/templates/{name}/raw | 获取一个 .gitignore 模板原始文件
[**getV5Licenses**](MiscellaneousApi.md#getV5Licenses) | **GET** v5/licenses | 列出可使用的开源许可协议
[**getV5LicensesLicense**](MiscellaneousApi.md#getV5LicensesLicense) | **GET** v5/licenses/{license} | 获取一个开源许可协议
[**getV5LicensesLicenseRaw**](MiscellaneousApi.md#getV5LicensesLicenseRaw) | **GET** v5/licenses/{license}/raw | 获取一个开源许可协议原始文件
[**getV5ReposOwnerRepoLicense**](MiscellaneousApi.md#getV5ReposOwnerRepoLicense) | **GET** v5/repos/{owner}/{repo}/license | 获取一个项目使用的开源许可协议
[**postV5Markdown**](MiscellaneousApi.md#postV5Markdown) | **POST** v5/markdown | 渲染 Markdown 文本


<a name="getV5Emojis"></a>
# **getV5Emojis**
> Void getV5Emojis(accessToken)

列出可使用的 Emoji

列出可使用的 Emoji

### Example
```java
// Import classes:
//import com.gitee.api.api.MiscellaneousApi;

MiscellaneousApi apiInstance =  new ApiClient().create(MiscellaneousApi.class);
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.getV5Emojis(accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5GitignoreTemplates"></a>
# **getV5GitignoreTemplates**
> Void getV5GitignoreTemplates(accessToken)

列出可使用的 .gitignore 模板

列出可使用的 .gitignore 模板

### Example
```java
// Import classes:
//import com.gitee.api.api.MiscellaneousApi;

MiscellaneousApi apiInstance =  new ApiClient().create(MiscellaneousApi.class);
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.getV5GitignoreTemplates(accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5GitignoreTemplatesName"></a>
# **getV5GitignoreTemplatesName**
> Void getV5GitignoreTemplatesName(name, accessToken)

获取一个 .gitignore 模板

获取一个 .gitignore 模板

### Example
```java
// Import classes:
//import com.gitee.api.api.MiscellaneousApi;

MiscellaneousApi apiInstance =  new ApiClient().create(MiscellaneousApi.class);
String name = "name_example"; // String | .gitignore 模板名
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.getV5GitignoreTemplatesName(name, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**| .gitignore 模板名 | [enum: Actionscript, Android, AppceleratorTitanium, Autotools, C, C++, CakePHP, CFWheels, Clojure, CMake, CodeIgniter, Composer, Concrete5, Coq, D, Dart, Delphi, Drupal, Eagle, Elixir, Erlang, ExpressionEngine, ExtJs, Finale, ForceDotCom, FuelPHP, Gcov, Go, Gradle, Grails, GWT, Haskell, Java, Jboss, Jekyll, Joomla, Kohana, Kotlin, Laravel, Leiningen, LemonStand, Lilypond, Lithium, Lua, Magento, Maven, Nanoc, Node, Objective-C, OCaml, Opa, Opencart, OracleForms, Perl, PlayFramework, Plone, Python, Qooxdoo, Qt, R, ROS, Rails, RhodesRhomobile, Ruby, Rust, Sass, Scala, Sdcc, SeamGen, SketchUp, SugarCRM, Swift, Symfony, SymphonyCMS, Textpattern, TurboGears2, Typo3, Unity, VisualStudio, Waf, WordPress, Yii, ZendFramework, Archives, Ansible, CVS, Eclipse, Emacs, Espresso, FlexBuilder, JetBrains, Linux, macOS, Matlab, Mercurial, ModelSim, MonoDevelop, NetBeans, Redcar, Redis, SBT, SublimeText, SVN, Tags, TextMate, Vim, VirtualEnv, Windows, Xcode, XilinxISE]
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5GitignoreTemplatesNameRaw"></a>
# **getV5GitignoreTemplatesNameRaw**
> Void getV5GitignoreTemplatesNameRaw(name, accessToken)

获取一个 .gitignore 模板原始文件

获取一个 .gitignore 模板原始文件

### Example
```java
// Import classes:
//import com.gitee.api.api.MiscellaneousApi;

MiscellaneousApi apiInstance =  new ApiClient().create(MiscellaneousApi.class);
String name = "name_example"; // String | .gitignore 模板名
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.getV5GitignoreTemplatesNameRaw(name, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**| .gitignore 模板名 | [enum: Actionscript, Android, AppceleratorTitanium, Autotools, C, C++, CakePHP, CFWheels, Clojure, CMake, CodeIgniter, Composer, Concrete5, Coq, D, Dart, Delphi, Drupal, Eagle, Elixir, Erlang, ExpressionEngine, ExtJs, Finale, ForceDotCom, FuelPHP, Gcov, Go, Gradle, Grails, GWT, Haskell, Java, Jboss, Jekyll, Joomla, Kohana, Kotlin, Laravel, Leiningen, LemonStand, Lilypond, Lithium, Lua, Magento, Maven, Nanoc, Node, Objective-C, OCaml, Opa, Opencart, OracleForms, Perl, PlayFramework, Plone, Python, Qooxdoo, Qt, R, ROS, Rails, RhodesRhomobile, Ruby, Rust, Sass, Scala, Sdcc, SeamGen, SketchUp, SugarCRM, Swift, Symfony, SymphonyCMS, Textpattern, TurboGears2, Typo3, Unity, VisualStudio, Waf, WordPress, Yii, ZendFramework, Archives, Ansible, CVS, Eclipse, Emacs, Espresso, FlexBuilder, JetBrains, Linux, macOS, Matlab, Mercurial, ModelSim, MonoDevelop, NetBeans, Redcar, Redis, SBT, SublimeText, SVN, Tags, TextMate, Vim, VirtualEnv, Windows, Xcode, XilinxISE]
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5Licenses"></a>
# **getV5Licenses**
> Void getV5Licenses(accessToken)

列出可使用的开源许可协议

列出可使用的开源许可协议

### Example
```java
// Import classes:
//import com.gitee.api.api.MiscellaneousApi;

MiscellaneousApi apiInstance =  new ApiClient().create(MiscellaneousApi.class);
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.getV5Licenses(accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5LicensesLicense"></a>
# **getV5LicensesLicense**
> Void getV5LicensesLicense(license, accessToken)

获取一个开源许可协议

获取一个开源许可协议

### Example
```java
// Import classes:
//import com.gitee.api.api.MiscellaneousApi;

MiscellaneousApi apiInstance =  new ApiClient().create(MiscellaneousApi.class);
String license = "license_example"; // String | 协议名称
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.getV5LicensesLicense(license, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **license** | **String**| 协议名称 | [enum: Apache v2 License, MIT License, GPL v2, Artistic License 2.0, BSD 2-Clause License, Affero GPL, LGPL v2.1, BSD (3-Clause) License, Eclipse Public License v1.0, LGPL v3, Mozilla Public License Version 2.0, GPL v3, WTFPL, Zlib]
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5LicensesLicenseRaw"></a>
# **getV5LicensesLicenseRaw**
> Void getV5LicensesLicenseRaw(license, accessToken)

获取一个开源许可协议原始文件

获取一个开源许可协议原始文件

### Example
```java
// Import classes:
//import com.gitee.api.api.MiscellaneousApi;

MiscellaneousApi apiInstance =  new ApiClient().create(MiscellaneousApi.class);
String license = "license_example"; // String | 协议名称
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.getV5LicensesLicenseRaw(license, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **license** | **String**| 协议名称 | [enum: Apache v2 License, MIT License, GPL v2, Artistic License 2.0, BSD 2-Clause License, Affero GPL, LGPL v2.1, BSD (3-Clause) License, Eclipse Public License v1.0, LGPL v3, Mozilla Public License Version 2.0, GPL v3, WTFPL, Zlib]
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoLicense"></a>
# **getV5ReposOwnerRepoLicense**
> Void getV5ReposOwnerRepoLicense(owner, repo, accessToken)

获取一个项目使用的开源许可协议

获取一个项目使用的开源许可协议

### Example
```java
// Import classes:
//import com.gitee.api.api.MiscellaneousApi;

MiscellaneousApi apiInstance =  new ApiClient().create(MiscellaneousApi.class);
String owner = "owner_example"; // String | 用户名(username/login)
String repo = "repo_example"; // String | 项目路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.getV5ReposOwnerRepoLicense(owner, repo, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 用户名(username/login) |
 **repo** | **String**| 项目路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postV5Markdown"></a>
# **postV5Markdown**
> Void postV5Markdown(text, accessToken)

渲染 Markdown 文本

渲染 Markdown 文本

### Example
```java
// Import classes:
//import com.gitee.api.api.MiscellaneousApi;

MiscellaneousApi apiInstance =  new ApiClient().create(MiscellaneousApi.class);
String text = "text_example"; // String | Markdown 文本
String accessToken = "accessToken_example"; // String | 用户授权码
Observable<Void> result = apiInstance.postV5Markdown(text, accessToken);
result.subscribe(new Observer<Void>() {
    @Override
    public void onCompleted() {
        System.out.println("finish!");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onNext(Void response) {
        System.out.println(response);
    }
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **text** | **String**| Markdown 文本 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Void**](.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

