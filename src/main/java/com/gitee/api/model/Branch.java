package com.gitee.api.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * this is description
 */
public class Branch   {
  @SerializedName("name")
  private String name = null;

  @SerializedName("commit")
  private BranchCommit commit = null;

  @SerializedName("protected")
  private Boolean _protected = null;

  @SerializedName("protection_url")
  private String protectionUrl = null;

  public Branch name(String name) {
    this.name = name;
    return this;
  }

   /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(example = "master", value = "")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Branch commit(BranchCommit commit) {
    this.commit = commit;
    return this;
  }

   /**
   * Get commit
   * @return commit
  **/
  @ApiModelProperty(value = "")
  public BranchCommit getCommit() {
    return commit;
  }

  public void setCommit(BranchCommit commit) {
    this.commit = commit;
  }

  public Branch _protected(Boolean _protected) {
    this._protected = _protected;
    return this;
  }

   /**
   * Get _protected
   * @return _protected
  **/
  @ApiModelProperty(value = "")
  public Boolean isProtected() {
    return _protected;
  }

  public void setProtected(Boolean _protected) {
    this._protected = _protected;
  }

  public Branch protectionUrl(String protectionUrl) {
    this.protectionUrl = protectionUrl;
    return this;
  }

   /**
   * Get protectionUrl
   * @return protectionUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/repos/wuyu15255872976/gitee-client/branches/master/protection", value = "")
  public String getProtectionUrl() {
    return protectionUrl;
  }

  public void setProtectionUrl(String protectionUrl) {
    this.protectionUrl = protectionUrl;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Branch branch = (Branch) o;
    return Objects.equals(this.name, branch.name) &&
        Objects.equals(this.commit, branch.commit) &&
        Objects.equals(this._protected, branch._protected) &&
        Objects.equals(this.protectionUrl, branch.protectionUrl);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, commit, _protected, protectionUrl);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Branch {\n");
    
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    commit: ").append(toIndentedString(commit)).append("\n");
    sb.append("    _protected: ").append(toIndentedString(_protected)).append("\n");
    sb.append("    protectionUrl: ").append(toIndentedString(protectionUrl)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

