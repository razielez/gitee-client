package com.gitee.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

/**
 * CommitContentCommitParents
 */
public class CommitContentCommitParents {
    @SerializedName("sha")
    private String sha = null;

    @SerializedName("url")
    private String url = null;

    public CommitContentCommitParents sha(String sha) {
        this.sha = sha;
        return this;
    }

    /**
     * Get sha
     *
     * @return sha
     **/
    public String getSha() {
        return sha;
    }

    public void setSha(String sha) {
        this.sha = sha;
    }

    public CommitContentCommitParents url(String url) {
        this.url = url;
        return this;
    }

    /**
     * Get url
     *
     * @return url
     **/
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CommitContentCommitParents commitContentCommitParents = (CommitContentCommitParents) o;
        return Objects.equals(this.sha, commitContentCommitParents.sha) &&
                Objects.equals(this.url, commitContentCommitParents.url);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sha, url);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class CommitContentCommitParents {\n");

        sb.append("    sha: ").append(toIndentedString(sha)).append("\n");
        sb.append("    url: ").append(toIndentedString(url)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

