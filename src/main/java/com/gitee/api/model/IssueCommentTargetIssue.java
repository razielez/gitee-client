package com.gitee.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

/**
 * IssueCommentTargetIssue
 */
public class IssueCommentTargetIssue {
    @SerializedName("id")
    private Integer id = null;

    @SerializedName("number")
    private String number = null;

    @SerializedName("title")
    private String title = null;

    public IssueCommentTargetIssue id(Integer id) {
        this.id = id;
        return this;
    }

    /**
     * Get id
     *
     * @return id
     **/
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public IssueCommentTargetIssue number(String number) {
        this.number = number;
        return this;
    }

    /**
     * Get number
     *
     * @return number
     **/
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public IssueCommentTargetIssue title(String title) {
        this.title = title;
        return this;
    }

    /**
     * Get title
     *
     * @return title
     **/
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        IssueCommentTargetIssue issueCommentTargetIssue = (IssueCommentTargetIssue) o;
        return Objects.equals(this.id, issueCommentTargetIssue.id) &&
                Objects.equals(this.number, issueCommentTargetIssue.number) &&
                Objects.equals(this.title, issueCommentTargetIssue.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, number, title);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class IssueCommentTargetIssue {\n");

        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    number: ").append(toIndentedString(number)).append("\n");
        sb.append("    title: ").append(toIndentedString(title)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

