package com.gitee.api.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * RepositoriesCommitCommit
 */

@javax.annotation.Generated(value = "io.swagger.generator.online.JavaClientCodegenImpl", date = "2017-11-16T14:06:19.500+08:00")

public class RepositoriesCommitCommit   {
  @SerializedName("url")
  private String url = null;

  @SerializedName("author")
  private RepositoriesCommitCommitAuthor author = null;

  @SerializedName("commiter")
  private RepositoriesCommitCommitAuthor commiter = null;

  @SerializedName("message")
  private String message = null;

  @SerializedName("tree")
  private RepositoriesCommitCommitTree tree = null;

  public RepositoriesCommitCommit url(String url) {
    this.url = url;
    return this;
  }

   /**
   * Get url
   * @return url
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/repos/wuyu15255872976/project-manager/git/commits/da3541317df136be1968ad45d7bd0237d90470c8", value = "")
  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public RepositoriesCommitCommit author(RepositoriesCommitCommitAuthor author) {
    this.author = author;
    return this;
  }

   /**
   * Get author
   * @return author
  **/
  @ApiModelProperty(value = "")
  public RepositoriesCommitCommitAuthor getAuthor() {
    return author;
  }

  public void setAuthor(RepositoriesCommitCommitAuthor author) {
    this.author = author;
  }

  public RepositoriesCommitCommit commiter(RepositoriesCommitCommitAuthor commiter) {
    this.commiter = commiter;
    return this;
  }

   /**
   * Get commiter
   * @return commiter
  **/
  @ApiModelProperty(value = "")
  public RepositoriesCommitCommitAuthor getCommiter() {
    return commiter;
  }

  public void setCommiter(RepositoriesCommitCommitAuthor commiter) {
    this.commiter = commiter;
  }

  public RepositoriesCommitCommit message(String message) {
    this.message = message;
    return this;
  }

   /**
   * Get message
   * @return message
  **/
  @ApiModelProperty(example = "gitee", value = "")
  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public RepositoriesCommitCommit tree(RepositoriesCommitCommitTree tree) {
    this.tree = tree;
    return this;
  }

   /**
   * Get tree
   * @return tree
  **/
  @ApiModelProperty(value = "")
  public RepositoriesCommitCommitTree getTree() {
    return tree;
  }

  public void setTree(RepositoriesCommitCommitTree tree) {
    this.tree = tree;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RepositoriesCommitCommit repositoriesCommitCommit = (RepositoriesCommitCommit) o;
    return Objects.equals(this.url, repositoriesCommitCommit.url) &&
        Objects.equals(this.author, repositoriesCommitCommit.author) &&
        Objects.equals(this.commiter, repositoriesCommitCommit.commiter) &&
        Objects.equals(this.message, repositoriesCommitCommit.message) &&
        Objects.equals(this.tree, repositoriesCommitCommit.tree);
  }

  @Override
  public int hashCode() {
    return Objects.hash(url, author, commiter, message, tree);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class RepositoriesCommitCommit {\n");
    
    sb.append("    url: ").append(toIndentedString(url)).append("\n");
    sb.append("    author: ").append(toIndentedString(author)).append("\n");
    sb.append("    commiter: ").append(toIndentedString(commiter)).append("\n");
    sb.append("    message: ").append(toIndentedString(message)).append("\n");
    sb.append("    tree: ").append(toIndentedString(tree)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

