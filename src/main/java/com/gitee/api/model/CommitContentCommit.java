package com.gitee.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

/**
 * CommitContentCommit
 */
public class CommitContentCommit {
    @SerializedName("sha")
    private String sha = null;

    @SerializedName("author")
    private CommitContentCommitAuthor author = null;

    @SerializedName("committer")
    private CommitContentCommitAuthor committer = null;

    @SerializedName("message")
    private String message = null;

    @SerializedName("tree")
    private CommitContentCommitTree tree = null;

    @SerializedName("parents")

    private java.util.List<CommitContentCommitParents> parents = null;

    public CommitContentCommit sha(String sha) {
        this.sha = sha;
        return this;
    }

    /**
     * Get sha
     *
     * @return sha
     **/
    public String getSha() {
        return sha;
    }

    public void setSha(String sha) {
        this.sha = sha;
    }

    public CommitContentCommit author(CommitContentCommitAuthor author) {
        this.author = author;
        return this;
    }

    /**
     * Get author
     *
     * @return author
     **/
    public CommitContentCommitAuthor getAuthor() {
        return author;
    }

    public void setAuthor(CommitContentCommitAuthor author) {
        this.author = author;
    }

    public CommitContentCommit committer(CommitContentCommitAuthor committer) {
        this.committer = committer;
        return this;
    }

    /**
     * Get committer
     *
     * @return committer
     **/
    public CommitContentCommitAuthor getCommitter() {
        return committer;
    }

    public void setCommitter(CommitContentCommitAuthor committer) {
        this.committer = committer;
    }

    public CommitContentCommit message(String message) {
        this.message = message;
        return this;
    }

    /**
     * Get message
     *
     * @return message
     **/
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CommitContentCommit tree(CommitContentCommitTree tree) {
        this.tree = tree;
        return this;
    }

    /**
     * Get tree
     *
     * @return tree
     **/
    public CommitContentCommitTree getTree() {
        return tree;
    }

    public void setTree(CommitContentCommitTree tree) {
        this.tree = tree;
    }

    public CommitContentCommit parents(java.util.List<CommitContentCommitParents> parents) {
        this.parents = parents;
        return this;
    }

    public CommitContentCommit addParentsItem(CommitContentCommitParents parentsItem) {
        if (this.parents == null) {
            this.parents = new java.util.ArrayList<CommitContentCommitParents>();
        }
        this.parents.add(parentsItem);
        return this;
    }

    /**
     * Get parents
     *
     * @return parents
     **/
    public java.util.List<CommitContentCommitParents> getParents() {
        return parents;
    }

    public void setParents(java.util.List<CommitContentCommitParents> parents) {
        this.parents = parents;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CommitContentCommit commitContentCommit = (CommitContentCommit) o;
        return Objects.equals(this.sha, commitContentCommit.sha) &&
                Objects.equals(this.author, commitContentCommit.author) &&
                Objects.equals(this.committer, commitContentCommit.committer) &&
                Objects.equals(this.message, commitContentCommit.message) &&
                Objects.equals(this.tree, commitContentCommit.tree) &&
                Objects.equals(this.parents, commitContentCommit.parents);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sha, author, committer, message, tree, parents);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class CommitContentCommit {\n");

        sb.append("    sha: ").append(toIndentedString(sha)).append("\n");
        sb.append("    author: ").append(toIndentedString(author)).append("\n");
        sb.append("    committer: ").append(toIndentedString(committer)).append("\n");
        sb.append("    message: ").append(toIndentedString(message)).append("\n");
        sb.append("    tree: ").append(toIndentedString(tree)).append("\n");
        sb.append("    parents: ").append(toIndentedString(parents)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

