package com.gitee.api.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * UserNotificationRepository
 */
public class UserNotificationRepository   {
  @SerializedName("id")
  private Integer id = null;

  @SerializedName("full_name")
  private String fullName = null;

  @SerializedName("url")
  private String url = null;

  @SerializedName("path")
  private String path = null;

  @SerializedName("name")
  private String name = null;

  @SerializedName("owner")
  private UserNotificationRepositoryOwner owner = null;

  @SerializedName("description")
  private String description = null;

  @SerializedName("private")
  private Boolean _private = null;

  @SerializedName("fork")
  private Boolean fork = null;

  @SerializedName("html_url")
  private String htmlUrl = null;

  public UserNotificationRepository id(Integer id) {
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(example = "1566904", value = "")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public UserNotificationRepository fullName(String fullName) {
    this.fullName = fullName;
    return this;
  }

   /**
   * Get fullName
   * @return fullName
  **/
  @ApiModelProperty(example = "gjcapp/zf-data-all", value = "")
  public String getFullName() {
    return fullName;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  public UserNotificationRepository url(String url) {
    this.url = url;
    return this;
  }

   /**
   * Get url
   * @return url
  **/
  @ApiModelProperty(example = "https://gitee.com/api/v5/repos/gjcapp/zf-data-all", value = "")
  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public UserNotificationRepository path(String path) {
    this.path = path;
    return this;
  }

   /**
   * Get path
   * @return path
  **/
  @ApiModelProperty(example = "zf-data-all", value = "")
  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public UserNotificationRepository name(String name) {
    this.name = name;
    return this;
  }

   /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(example = "zf-data-all", value = "")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public UserNotificationRepository owner(UserNotificationRepositoryOwner owner) {
    this.owner = owner;
    return this;
  }

   /**
   * Get owner
   * @return owner
  **/
  @ApiModelProperty(value = "")
  public UserNotificationRepositoryOwner getOwner() {
    return owner;
  }

  public void setOwner(UserNotificationRepositoryOwner owner) {
    this.owner = owner;
  }

  public UserNotificationRepository description(String description) {
    this.description = description;
    return this;
  }

   /**
   * Get description
   * @return description
  **/
  @ApiModelProperty(example = "", value = "")
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public UserNotificationRepository _private(Boolean _private) {
    this._private = _private;
    return this;
  }

   /**
   * Get _private
   * @return _private
  **/
  @ApiModelProperty(value = "")
  public Boolean isPrivate() {
    return _private;
  }

  public void setPrivate(Boolean _private) {
    this._private = _private;
  }

  public UserNotificationRepository fork(Boolean fork) {
    this.fork = fork;
    return this;
  }

   /**
   * Get fork
   * @return fork
  **/
  @ApiModelProperty(value = "")
  public Boolean isFork() {
    return fork;
  }

  public void setFork(Boolean fork) {
    this.fork = fork;
  }

  public UserNotificationRepository htmlUrl(String htmlUrl) {
    this.htmlUrl = htmlUrl;
    return this;
  }

   /**
   * Get htmlUrl
   * @return htmlUrl
  **/
  @ApiModelProperty(example = "https://gitee.com/gjcapp/zf-data-all", value = "")
  public String getHtmlUrl() {
    return htmlUrl;
  }

  public void setHtmlUrl(String htmlUrl) {
    this.htmlUrl = htmlUrl;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UserNotificationRepository userNotificationRepository = (UserNotificationRepository) o;
    return Objects.equals(this.id, userNotificationRepository.id) &&
        Objects.equals(this.fullName, userNotificationRepository.fullName) &&
        Objects.equals(this.url, userNotificationRepository.url) &&
        Objects.equals(this.path, userNotificationRepository.path) &&
        Objects.equals(this.name, userNotificationRepository.name) &&
        Objects.equals(this.owner, userNotificationRepository.owner) &&
        Objects.equals(this.description, userNotificationRepository.description) &&
        Objects.equals(this._private, userNotificationRepository._private) &&
        Objects.equals(this.fork, userNotificationRepository.fork) &&
        Objects.equals(this.htmlUrl, userNotificationRepository.htmlUrl);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, fullName, url, path, name, owner, description, _private, fork, htmlUrl);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UserNotificationRepository {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    fullName: ").append(toIndentedString(fullName)).append("\n");
    sb.append("    url: ").append(toIndentedString(url)).append("\n");
    sb.append("    path: ").append(toIndentedString(path)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    owner: ").append(toIndentedString(owner)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    _private: ").append(toIndentedString(_private)).append("\n");
    sb.append("    fork: ").append(toIndentedString(fork)).append("\n");
    sb.append("    htmlUrl: ").append(toIndentedString(htmlUrl)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

