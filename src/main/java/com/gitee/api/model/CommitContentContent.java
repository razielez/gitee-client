package com.gitee.api.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * CommitContentContent
 */
public class CommitContentContent {
    @SerializedName("name")
    private String name = null;

    @SerializedName("path")
    private String path = null;

    @SerializedName("size")
    private Integer size = null;

    @SerializedName("sha")
    private String sha = null;

    @SerializedName("type")
    private String type = null;

    @SerializedName("url")
    private String url = null;

    @SerializedName("html_url")
    private String htmlUrl = null;

    @SerializedName("download_url")
    private String downloadUrl = null;

    @SerializedName("_links")
    private CommitContentContentLinks links = null;

    public CommitContentContent name(String name) {
        this.name = name;
        return this;
    }

    /**
     * Get name
     *
     * @return name
     **/
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CommitContentContent path(String path) {
        this.path = path;
        return this;
    }

    /**
     * Get path
     *
     * @return path
     **/
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public CommitContentContent size(Integer size) {
        this.size = size;
        return this;
    }

    /**
     * Get size
     *
     * @return size
     **/
    @ApiModelProperty(example = "3", value = "")
    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public CommitContentContent sha(String sha) {
        this.sha = sha;
        return this;
    }

    /**
     * Get sha
     *
     * @return sha
     **/
    public String getSha() {
        return sha;
    }

    public void setSha(String sha) {
        this.sha = sha;
    }

    public CommitContentContent type(String type) {
        this.type = type;
        return this;
    }

    /**
     * Get type
     *
     * @return type
     **/
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public CommitContentContent url(String url) {
        this.url = url;
        return this;
    }

    /**
     * Get url
     *
     * @return url
     **/
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public CommitContentContent htmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
        return this;
    }

    /**
     * Get htmlUrl
     *
     * @return htmlUrl
     **/
    public String getHtmlUrl() {
        return htmlUrl;
    }

    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
    }

    public CommitContentContent downloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
        return this;
    }

    /**
     * Get downloadUrl
     *
     * @return downloadUrl
     **/
    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public CommitContentContent links(CommitContentContentLinks links) {
        this.links = links;
        return this;
    }

    /**
     * Get links
     *
     * @return links
     **/
    public CommitContentContentLinks getLinks() {
        return links;
    }

    public void setLinks(CommitContentContentLinks links) {
        this.links = links;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CommitContentContent commitContentContent = (CommitContentContent) o;
        return Objects.equals(this.name, commitContentContent.name) &&
                Objects.equals(this.path, commitContentContent.path) &&
                Objects.equals(this.size, commitContentContent.size) &&
                Objects.equals(this.sha, commitContentContent.sha) &&
                Objects.equals(this.type, commitContentContent.type) &&
                Objects.equals(this.url, commitContentContent.url) &&
                Objects.equals(this.htmlUrl, commitContentContent.htmlUrl) &&
                Objects.equals(this.downloadUrl, commitContentContent.downloadUrl) &&
                Objects.equals(this.links, commitContentContent.links);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, path, size, sha, type, url, htmlUrl, downloadUrl, links);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class CommitContentContent {\n");

        sb.append("    name: ").append(toIndentedString(name)).append("\n");
        sb.append("    path: ").append(toIndentedString(path)).append("\n");
        sb.append("    size: ").append(toIndentedString(size)).append("\n");
        sb.append("    sha: ").append(toIndentedString(sha)).append("\n");
        sb.append("    type: ").append(toIndentedString(type)).append("\n");
        sb.append("    url: ").append(toIndentedString(url)).append("\n");
        sb.append("    htmlUrl: ").append(toIndentedString(htmlUrl)).append("\n");
        sb.append("    downloadUrl: ").append(toIndentedString(downloadUrl)).append("\n");
        sb.append("    links: ").append(toIndentedString(links)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

