package com.gitee.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

/**
 * this is description
 */
public class IssueComment   {
  @SerializedName("id")
  private Integer id = null;

  @SerializedName("body")
  private String body = null;

  @SerializedName("user")
  private IssueCommentUser user = null;

  @SerializedName("source")
  private String source = null;

  @SerializedName("created_at")
  private java.util.Date createdAt = null;

  @SerializedName("target")
  private IssueCommentTarget target = null;

  public IssueComment id(Integer id) {
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public IssueComment body(String body) {
    this.body = body;
    return this;
  }

   /**
   * Get body
   * @return body
  **/
  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public IssueComment user(IssueCommentUser user) {
    this.user = user;
    return this;
  }

   /**
   * Get user
   * @return user
  **/
  public IssueCommentUser getUser() {
    return user;
  }

  public void setUser(IssueCommentUser user) {
    this.user = user;
  }

  public IssueComment source(String source) {
    this.source = source;
    return this;
  }

   /**
   * Get source
   * @return source
  **/
  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public IssueComment createdAt(java.util.Date createdAt) {
    this.createdAt = createdAt;
    return this;
  }

   /**
   * Get createdAt
   * @return createdAt
  **/
  public java.util.Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(java.util.Date createdAt) {
    this.createdAt = createdAt;
  }

  public IssueComment target(IssueCommentTarget target) {
    this.target = target;
    return this;
  }

   /**
   * Get target
   * @return target
  **/
  public IssueCommentTarget getTarget() {
    return target;
  }

  public void setTarget(IssueCommentTarget target) {
    this.target = target;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    IssueComment issueComment = (IssueComment) o;
    return Objects.equals(this.id, issueComment.id) &&
        Objects.equals(this.body, issueComment.body) &&
        Objects.equals(this.user, issueComment.user) &&
        Objects.equals(this.source, issueComment.source) &&
        Objects.equals(this.createdAt, issueComment.createdAt) &&
        Objects.equals(this.target, issueComment.target);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, body, user, source, createdAt, target);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class IssueComment {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    body: ").append(toIndentedString(body)).append("\n");
    sb.append("    user: ").append(toIndentedString(user)).append("\n");
    sb.append("    source: ").append(toIndentedString(source)).append("\n");
    sb.append("    createdAt: ").append(toIndentedString(createdAt)).append("\n");
    sb.append("    target: ").append(toIndentedString(target)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

