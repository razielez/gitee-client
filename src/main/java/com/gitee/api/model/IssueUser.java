package com.gitee.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

/**
 * IssueUser
 */
public class IssueUser {
    @SerializedName("login")
    private String login = null;

    @SerializedName("id")
    private Integer id = null;

    @SerializedName("avatar_url")
    private String avatarUrl = null;

    @SerializedName("url")
    private String url = null;

    @SerializedName("html_url")
    private String htmlUrl = null;

    @SerializedName("followers_url")
    private String followersUrl = null;

    @SerializedName("following_url")
    private String followingUrl = null;

    @SerializedName("gists_url")
    private String gistsUrl = null;

    @SerializedName("starred_url")
    private String starredUrl = null;

    @SerializedName("subscriptions_url")
    private String subscriptionsUrl = null;

    @SerializedName("organizations_url")
    private String organizationsUrl = null;

    @SerializedName("repos_url")
    private String reposUrl = null;

    @SerializedName("events_url")
    private String eventsUrl = null;

    @SerializedName("received_events_url")
    private String receivedEventsUrl = null;

    @SerializedName("type")
    private String type = null;

    @SerializedName("site_admin")
    private Boolean siteAdmin = null;

    public IssueUser login(String login) {
        this.login = login;
        return this;
    }

    /**
     * Get login
     *
     * @return login
     **/
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public IssueUser id(Integer id) {
        this.id = id;
        return this;
    }

    /**
     * Get id
     *
     * @return id
     **/
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public IssueUser avatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
        return this;
    }

    /**
     * Get avatarUrl
     *
     * @return avatarUrl
     **/
    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public IssueUser url(String url) {
        this.url = url;
        return this;
    }

    /**
     * Get url
     *
     * @return url
     **/
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public IssueUser htmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
        return this;
    }

    /**
     * Get htmlUrl
     *
     * @return htmlUrl
     **/
    public String getHtmlUrl() {
        return htmlUrl;
    }

    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
    }

    public IssueUser followersUrl(String followersUrl) {
        this.followersUrl = followersUrl;
        return this;
    }

    /**
     * Get followersUrl
     *
     * @return followersUrl
     **/
    public String getFollowersUrl() {
        return followersUrl;
    }

    public void setFollowersUrl(String followersUrl) {
        this.followersUrl = followersUrl;
    }

    public IssueUser followingUrl(String followingUrl) {
        this.followingUrl = followingUrl;
        return this;
    }

    /**
     * Get followingUrl
     *
     * @return followingUrl
     **/
    public String getFollowingUrl() {
        return followingUrl;
    }

    public void setFollowingUrl(String followingUrl) {
        this.followingUrl = followingUrl;
    }

    public IssueUser gistsUrl(String gistsUrl) {
        this.gistsUrl = gistsUrl;
        return this;
    }

    /**
     * Get gistsUrl
     *
     * @return gistsUrl
     **/
    public String getGistsUrl() {
        return gistsUrl;
    }

    public void setGistsUrl(String gistsUrl) {
        this.gistsUrl = gistsUrl;
    }

    public IssueUser starredUrl(String starredUrl) {
        this.starredUrl = starredUrl;
        return this;
    }

    /**
     * Get starredUrl
     *
     * @return starredUrl
     **/
    public String getStarredUrl() {
        return starredUrl;
    }

    public void setStarredUrl(String starredUrl) {
        this.starredUrl = starredUrl;
    }

    public IssueUser subscriptionsUrl(String subscriptionsUrl) {
        this.subscriptionsUrl = subscriptionsUrl;
        return this;
    }

    /**
     * Get subscriptionsUrl
     *
     * @return subscriptionsUrl
     **/
    public String getSubscriptionsUrl() {
        return subscriptionsUrl;
    }

    public void setSubscriptionsUrl(String subscriptionsUrl) {
        this.subscriptionsUrl = subscriptionsUrl;
    }

    public IssueUser organizationsUrl(String organizationsUrl) {
        this.organizationsUrl = organizationsUrl;
        return this;
    }

    /**
     * Get organizationsUrl
     *
     * @return organizationsUrl
     **/
    public String getOrganizationsUrl() {
        return organizationsUrl;
    }

    public void setOrganizationsUrl(String organizationsUrl) {
        this.organizationsUrl = organizationsUrl;
    }

    public IssueUser reposUrl(String reposUrl) {
        this.reposUrl = reposUrl;
        return this;
    }

    /**
     * Get reposUrl
     *
     * @return reposUrl
     **/
    public String getReposUrl() {
        return reposUrl;
    }

    public void setReposUrl(String reposUrl) {
        this.reposUrl = reposUrl;
    }

    public IssueUser eventsUrl(String eventsUrl) {
        this.eventsUrl = eventsUrl;
        return this;
    }

    /**
     * Get eventsUrl
     *
     * @return eventsUrl
     **/
    public String getEventsUrl() {
        return eventsUrl;
    }

    public void setEventsUrl(String eventsUrl) {
        this.eventsUrl = eventsUrl;
    }

    public IssueUser receivedEventsUrl(String receivedEventsUrl) {
        this.receivedEventsUrl = receivedEventsUrl;
        return this;
    }

    /**
     * Get receivedEventsUrl
     *
     * @return receivedEventsUrl
     **/
    public String getReceivedEventsUrl() {
        return receivedEventsUrl;
    }

    public void setReceivedEventsUrl(String receivedEventsUrl) {
        this.receivedEventsUrl = receivedEventsUrl;
    }

    public IssueUser type(String type) {
        this.type = type;
        return this;
    }

    /**
     * Get type
     *
     * @return type
     **/
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public IssueUser siteAdmin(Boolean siteAdmin) {
        this.siteAdmin = siteAdmin;
        return this;
    }

    /**
     * Get siteAdmin
     *
     * @return siteAdmin
     **/
    public Boolean isSiteAdmin() {
        return siteAdmin;
    }

    public void setSiteAdmin(Boolean siteAdmin) {
        this.siteAdmin = siteAdmin;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        IssueUser issueUser = (IssueUser) o;
        return Objects.equals(this.login, issueUser.login) &&
                Objects.equals(this.id, issueUser.id) &&
                Objects.equals(this.avatarUrl, issueUser.avatarUrl) &&
                Objects.equals(this.url, issueUser.url) &&
                Objects.equals(this.htmlUrl, issueUser.htmlUrl) &&
                Objects.equals(this.followersUrl, issueUser.followersUrl) &&
                Objects.equals(this.followingUrl, issueUser.followingUrl) &&
                Objects.equals(this.gistsUrl, issueUser.gistsUrl) &&
                Objects.equals(this.starredUrl, issueUser.starredUrl) &&
                Objects.equals(this.subscriptionsUrl, issueUser.subscriptionsUrl) &&
                Objects.equals(this.organizationsUrl, issueUser.organizationsUrl) &&
                Objects.equals(this.reposUrl, issueUser.reposUrl) &&
                Objects.equals(this.eventsUrl, issueUser.eventsUrl) &&
                Objects.equals(this.receivedEventsUrl, issueUser.receivedEventsUrl) &&
                Objects.equals(this.type, issueUser.type) &&
                Objects.equals(this.siteAdmin, issueUser.siteAdmin);
    }

    @Override
    public int hashCode() {
        return Objects.hash(login, id, avatarUrl, url, htmlUrl, followersUrl, followingUrl, gistsUrl, starredUrl, subscriptionsUrl, organizationsUrl, reposUrl, eventsUrl, receivedEventsUrl, type, siteAdmin);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class IssueUser {\n");

        sb.append("    login: ").append(toIndentedString(login)).append("\n");
        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    avatarUrl: ").append(toIndentedString(avatarUrl)).append("\n");
        sb.append("    url: ").append(toIndentedString(url)).append("\n");
        sb.append("    htmlUrl: ").append(toIndentedString(htmlUrl)).append("\n");
        sb.append("    followersUrl: ").append(toIndentedString(followersUrl)).append("\n");
        sb.append("    followingUrl: ").append(toIndentedString(followingUrl)).append("\n");
        sb.append("    gistsUrl: ").append(toIndentedString(gistsUrl)).append("\n");
        sb.append("    starredUrl: ").append(toIndentedString(starredUrl)).append("\n");
        sb.append("    subscriptionsUrl: ").append(toIndentedString(subscriptionsUrl)).append("\n");
        sb.append("    organizationsUrl: ").append(toIndentedString(organizationsUrl)).append("\n");
        sb.append("    reposUrl: ").append(toIndentedString(reposUrl)).append("\n");
        sb.append("    eventsUrl: ").append(toIndentedString(eventsUrl)).append("\n");
        sb.append("    receivedEventsUrl: ").append(toIndentedString(receivedEventsUrl)).append("\n");
        sb.append("    type: ").append(toIndentedString(type)).append("\n");
        sb.append("    siteAdmin: ").append(toIndentedString(siteAdmin)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

