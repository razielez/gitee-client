package com.gitee.api.model;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * ProjectParent
 */
public class ProjectParent {
    @SerializedName("id")
    private String id = null;

    @SerializedName("full_name")
    private String fullName = null;

    @SerializedName("url")
    private String url = null;

    @SerializedName("path")
    private String path = null;

    @SerializedName("name")
    private String name = null;

    @SerializedName("owner")
    private String owner = null;

    @SerializedName("description")
    private String description = null;

    @SerializedName("private")
    private String _private = null;

    @SerializedName("fork")
    private String fork = null;

    @SerializedName("html_url")
    private String htmlUrl = null;

    @SerializedName("forks_url")
    private String forksUrl = null;

    @SerializedName("keys_url")
    private String keysUrl = null;

    @SerializedName("collaborators_url")
    private String collaboratorsUrl = null;

    @SerializedName("hooks_url")
    private String hooksUrl = null;

    @SerializedName("branches_url")
    private String branchesUrl = null;

    @SerializedName("tags_url")
    private String tagsUrl = null;

    @SerializedName("blobs_url")
    private String blobsUrl = null;

    @SerializedName("stargazers_url")
    private String stargazersUrl = null;

    @SerializedName("contributors_url")
    private String contributorsUrl = null;

    @SerializedName("commits_url")
    private String commitsUrl = null;

    @SerializedName("comments_url")
    private String commentsUrl = null;

    @SerializedName("issue_comment_url")
    private String issueCommentUrl = null;

    @SerializedName("issues_url")
    private String issuesUrl = null;

    @SerializedName("pulls_url")
    private String pullsUrl = null;

    @SerializedName("milestones_url")
    private String milestonesUrl = null;

    @SerializedName("notifications_url")
    private String notificationsUrl = null;

    @SerializedName("labels_url")
    private String labelsUrl = null;

    @SerializedName("releases_url")
    private String releasesUrl = null;

    @SerializedName("recommend")
    private String recommend = null;

    @SerializedName("homepage")
    private String homepage = null;

    @SerializedName("language")
    private String language = null;

    @SerializedName("forks_count")
    private String forksCount = null;

    @SerializedName("stargazers_count")
    private String stargazersCount = null;

    @SerializedName("watchers_count")
    private String watchersCount = null;

    @SerializedName("default_branch")
    private String defaultBranch = null;

    @SerializedName("open_issues_count")
    private String openIssuesCount = null;

    @SerializedName("has_issues")
    private String hasIssues = null;

    @SerializedName("has_wiki")
    private String hasWiki = null;

    @SerializedName("pull_requests_enabled")
    private String pullRequestsEnabled = null;

    @SerializedName("has_page")
    private String hasPage = null;

    @SerializedName("license")
    private String license = null;

    @SerializedName("pushed_at")
    private String pushedAt = null;

    @SerializedName("created_at")
    private String createdAt = null;

    @SerializedName("updated_at")
    private String updatedAt = null;

    @SerializedName("paas")
    private String paas = null;

    @SerializedName("stared")
    private String stared = null;

    @SerializedName("watched")
    private String watched = null;

    @SerializedName("permission")
    private String permission = null;

    @SerializedName("relation")
    private String relation = null;

    public ProjectParent id(String id) {
        this.id = id;
        return this;
    }

    /**
     * Get id
     *
     * @return id
     **/
    @ApiModelProperty(example = "integer", value = "")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ProjectParent fullName(String fullName) {
        this.fullName = fullName;
        return this;
    }

    /**
     * Get fullName
     *
     * @return fullName
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public ProjectParent url(String url) {
        this.url = url;
        return this;
    }

    /**
     * Get url
     *
     * @return url
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ProjectParent path(String path) {
        this.path = path;
        return this;
    }

    /**
     * Get path
     *
     * @return path
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public ProjectParent name(String name) {
        this.name = name;
        return this;
    }

    /**
     * Get name
     *
     * @return name
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProjectParent owner(String owner) {
        this.owner = owner;
        return this;
    }

    /**
     * Get owner
     *
     * @return owner
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public ProjectParent description(String description) {
        this.description = description;
        return this;
    }

    /**
     * Get description
     *
     * @return description
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ProjectParent _private(String _private) {
        this._private = _private;
        return this;
    }

    /**
     * Get _private
     *
     * @return _private
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getPrivate() {
        return _private;
    }

    public void setPrivate(String _private) {
        this._private = _private;
    }

    public ProjectParent fork(String fork) {
        this.fork = fork;
        return this;
    }

    /**
     * Get fork
     *
     * @return fork
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getFork() {
        return fork;
    }

    public void setFork(String fork) {
        this.fork = fork;
    }

    public ProjectParent htmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
        return this;
    }

    /**
     * Get htmlUrl
     *
     * @return htmlUrl
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getHtmlUrl() {
        return htmlUrl;
    }

    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
    }

    public ProjectParent forksUrl(String forksUrl) {
        this.forksUrl = forksUrl;
        return this;
    }

    /**
     * Get forksUrl
     *
     * @return forksUrl
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getForksUrl() {
        return forksUrl;
    }

    public void setForksUrl(String forksUrl) {
        this.forksUrl = forksUrl;
    }

    public ProjectParent keysUrl(String keysUrl) {
        this.keysUrl = keysUrl;
        return this;
    }

    /**
     * Get keysUrl
     *
     * @return keysUrl
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getKeysUrl() {
        return keysUrl;
    }

    public void setKeysUrl(String keysUrl) {
        this.keysUrl = keysUrl;
    }

    public ProjectParent collaboratorsUrl(String collaboratorsUrl) {
        this.collaboratorsUrl = collaboratorsUrl;
        return this;
    }

    /**
     * Get collaboratorsUrl
     *
     * @return collaboratorsUrl
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getCollaboratorsUrl() {
        return collaboratorsUrl;
    }

    public void setCollaboratorsUrl(String collaboratorsUrl) {
        this.collaboratorsUrl = collaboratorsUrl;
    }

    public ProjectParent hooksUrl(String hooksUrl) {
        this.hooksUrl = hooksUrl;
        return this;
    }

    /**
     * Get hooksUrl
     *
     * @return hooksUrl
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getHooksUrl() {
        return hooksUrl;
    }

    public void setHooksUrl(String hooksUrl) {
        this.hooksUrl = hooksUrl;
    }

    public ProjectParent branchesUrl(String branchesUrl) {
        this.branchesUrl = branchesUrl;
        return this;
    }

    /**
     * Get branchesUrl
     *
     * @return branchesUrl
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getBranchesUrl() {
        return branchesUrl;
    }

    public void setBranchesUrl(String branchesUrl) {
        this.branchesUrl = branchesUrl;
    }

    public ProjectParent tagsUrl(String tagsUrl) {
        this.tagsUrl = tagsUrl;
        return this;
    }

    /**
     * Get tagsUrl
     *
     * @return tagsUrl
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getTagsUrl() {
        return tagsUrl;
    }

    public void setTagsUrl(String tagsUrl) {
        this.tagsUrl = tagsUrl;
    }

    public ProjectParent blobsUrl(String blobsUrl) {
        this.blobsUrl = blobsUrl;
        return this;
    }

    /**
     * Get blobsUrl
     *
     * @return blobsUrl
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getBlobsUrl() {
        return blobsUrl;
    }

    public void setBlobsUrl(String blobsUrl) {
        this.blobsUrl = blobsUrl;
    }

    public ProjectParent stargazersUrl(String stargazersUrl) {
        this.stargazersUrl = stargazersUrl;
        return this;
    }

    /**
     * Get stargazersUrl
     *
     * @return stargazersUrl
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getStargazersUrl() {
        return stargazersUrl;
    }

    public void setStargazersUrl(String stargazersUrl) {
        this.stargazersUrl = stargazersUrl;
    }

    public ProjectParent contributorsUrl(String contributorsUrl) {
        this.contributorsUrl = contributorsUrl;
        return this;
    }

    /**
     * Get contributorsUrl
     *
     * @return contributorsUrl
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getContributorsUrl() {
        return contributorsUrl;
    }

    public void setContributorsUrl(String contributorsUrl) {
        this.contributorsUrl = contributorsUrl;
    }

    public ProjectParent commitsUrl(String commitsUrl) {
        this.commitsUrl = commitsUrl;
        return this;
    }

    /**
     * Get commitsUrl
     *
     * @return commitsUrl
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getCommitsUrl() {
        return commitsUrl;
    }

    public void setCommitsUrl(String commitsUrl) {
        this.commitsUrl = commitsUrl;
    }

    public ProjectParent commentsUrl(String commentsUrl) {
        this.commentsUrl = commentsUrl;
        return this;
    }

    /**
     * Get commentsUrl
     *
     * @return commentsUrl
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getCommentsUrl() {
        return commentsUrl;
    }

    public void setCommentsUrl(String commentsUrl) {
        this.commentsUrl = commentsUrl;
    }

    public ProjectParent issueCommentUrl(String issueCommentUrl) {
        this.issueCommentUrl = issueCommentUrl;
        return this;
    }

    /**
     * Get issueCommentUrl
     *
     * @return issueCommentUrl
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getIssueCommentUrl() {
        return issueCommentUrl;
    }

    public void setIssueCommentUrl(String issueCommentUrl) {
        this.issueCommentUrl = issueCommentUrl;
    }

    public ProjectParent issuesUrl(String issuesUrl) {
        this.issuesUrl = issuesUrl;
        return this;
    }

    /**
     * Get issuesUrl
     *
     * @return issuesUrl
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getIssuesUrl() {
        return issuesUrl;
    }

    public void setIssuesUrl(String issuesUrl) {
        this.issuesUrl = issuesUrl;
    }

    public ProjectParent pullsUrl(String pullsUrl) {
        this.pullsUrl = pullsUrl;
        return this;
    }

    /**
     * Get pullsUrl
     *
     * @return pullsUrl
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getPullsUrl() {
        return pullsUrl;
    }

    public void setPullsUrl(String pullsUrl) {
        this.pullsUrl = pullsUrl;
    }

    public ProjectParent milestonesUrl(String milestonesUrl) {
        this.milestonesUrl = milestonesUrl;
        return this;
    }

    /**
     * Get milestonesUrl
     *
     * @return milestonesUrl
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getMilestonesUrl() {
        return milestonesUrl;
    }

    public void setMilestonesUrl(String milestonesUrl) {
        this.milestonesUrl = milestonesUrl;
    }

    public ProjectParent notificationsUrl(String notificationsUrl) {
        this.notificationsUrl = notificationsUrl;
        return this;
    }

    /**
     * Get notificationsUrl
     *
     * @return notificationsUrl
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getNotificationsUrl() {
        return notificationsUrl;
    }

    public void setNotificationsUrl(String notificationsUrl) {
        this.notificationsUrl = notificationsUrl;
    }

    public ProjectParent labelsUrl(String labelsUrl) {
        this.labelsUrl = labelsUrl;
        return this;
    }

    /**
     * Get labelsUrl
     *
     * @return labelsUrl
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getLabelsUrl() {
        return labelsUrl;
    }

    public void setLabelsUrl(String labelsUrl) {
        this.labelsUrl = labelsUrl;
    }

    public ProjectParent releasesUrl(String releasesUrl) {
        this.releasesUrl = releasesUrl;
        return this;
    }

    /**
     * Get releasesUrl
     *
     * @return releasesUrl
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getReleasesUrl() {
        return releasesUrl;
    }

    public void setReleasesUrl(String releasesUrl) {
        this.releasesUrl = releasesUrl;
    }

    public ProjectParent recommend(String recommend) {
        this.recommend = recommend;
        return this;
    }

    /**
     * Get recommend
     *
     * @return recommend
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getRecommend() {
        return recommend;
    }

    public void setRecommend(String recommend) {
        this.recommend = recommend;
    }

    public ProjectParent homepage(String homepage) {
        this.homepage = homepage;
        return this;
    }

    /**
     * Get homepage
     *
     * @return homepage
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public ProjectParent language(String language) {
        this.language = language;
        return this;
    }

    /**
     * Get language
     *
     * @return language
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public ProjectParent forksCount(String forksCount) {
        this.forksCount = forksCount;
        return this;
    }

    /**
     * Get forksCount
     *
     * @return forksCount
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getForksCount() {
        return forksCount;
    }

    public void setForksCount(String forksCount) {
        this.forksCount = forksCount;
    }

    public ProjectParent stargazersCount(String stargazersCount) {
        this.stargazersCount = stargazersCount;
        return this;
    }

    /**
     * Get stargazersCount
     *
     * @return stargazersCount
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getStargazersCount() {
        return stargazersCount;
    }

    public void setStargazersCount(String stargazersCount) {
        this.stargazersCount = stargazersCount;
    }

    public ProjectParent watchersCount(String watchersCount) {
        this.watchersCount = watchersCount;
        return this;
    }

    /**
     * Get watchersCount
     *
     * @return watchersCount
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getWatchersCount() {
        return watchersCount;
    }

    public void setWatchersCount(String watchersCount) {
        this.watchersCount = watchersCount;
    }

    public ProjectParent defaultBranch(String defaultBranch) {
        this.defaultBranch = defaultBranch;
        return this;
    }

    /**
     * Get defaultBranch
     *
     * @return defaultBranch
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getDefaultBranch() {
        return defaultBranch;
    }

    public void setDefaultBranch(String defaultBranch) {
        this.defaultBranch = defaultBranch;
    }

    public ProjectParent openIssuesCount(String openIssuesCount) {
        this.openIssuesCount = openIssuesCount;
        return this;
    }

    /**
     * Get openIssuesCount
     *
     * @return openIssuesCount
     **/
    @ApiModelProperty(example = "integer", value = "")
    public String getOpenIssuesCount() {
        return openIssuesCount;
    }

    public void setOpenIssuesCount(String openIssuesCount) {
        this.openIssuesCount = openIssuesCount;
    }

    public ProjectParent hasIssues(String hasIssues) {
        this.hasIssues = hasIssues;
        return this;
    }

    /**
     * Get hasIssues
     *
     * @return hasIssues
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getHasIssues() {
        return hasIssues;
    }

    public void setHasIssues(String hasIssues) {
        this.hasIssues = hasIssues;
    }

    public ProjectParent hasWiki(String hasWiki) {
        this.hasWiki = hasWiki;
        return this;
    }

    /**
     * Get hasWiki
     *
     * @return hasWiki
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getHasWiki() {
        return hasWiki;
    }

    public void setHasWiki(String hasWiki) {
        this.hasWiki = hasWiki;
    }

    public ProjectParent pullRequestsEnabled(String pullRequestsEnabled) {
        this.pullRequestsEnabled = pullRequestsEnabled;
        return this;
    }

    /**
     * Get pullRequestsEnabled
     *
     * @return pullRequestsEnabled
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getPullRequestsEnabled() {
        return pullRequestsEnabled;
    }

    public void setPullRequestsEnabled(String pullRequestsEnabled) {
        this.pullRequestsEnabled = pullRequestsEnabled;
    }

    public ProjectParent hasPage(String hasPage) {
        this.hasPage = hasPage;
        return this;
    }

    /**
     * Get hasPage
     *
     * @return hasPage
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getHasPage() {
        return hasPage;
    }

    public void setHasPage(String hasPage) {
        this.hasPage = hasPage;
    }

    public ProjectParent license(String license) {
        this.license = license;
        return this;
    }

    /**
     * Get license
     *
     * @return license
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public ProjectParent pushedAt(String pushedAt) {
        this.pushedAt = pushedAt;
        return this;
    }

    /**
     * Get pushedAt
     *
     * @return pushedAt
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getPushedAt() {
        return pushedAt;
    }

    public void setPushedAt(String pushedAt) {
        this.pushedAt = pushedAt;
    }

    public ProjectParent createdAt(String createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    /**
     * Get createdAt
     *
     * @return createdAt
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public ProjectParent updatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    /**
     * Get updatedAt
     *
     * @return updatedAt
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public ProjectParent paas(String paas) {
        this.paas = paas;
        return this;
    }

    /**
     * Get paas
     *
     * @return paas
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getPaas() {
        return paas;
    }

    public void setPaas(String paas) {
        this.paas = paas;
    }

    public ProjectParent stared(String stared) {
        this.stared = stared;
        return this;
    }

    /**
     * Get stared
     *
     * @return stared
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getStared() {
        return stared;
    }

    public void setStared(String stared) {
        this.stared = stared;
    }

    public ProjectParent watched(String watched) {
        this.watched = watched;
        return this;
    }

    /**
     * Get watched
     *
     * @return watched
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getWatched() {
        return watched;
    }

    public void setWatched(String watched) {
        this.watched = watched;
    }

    public ProjectParent permission(String permission) {
        this.permission = permission;
        return this;
    }

    /**
     * Get permission
     *
     * @return permission
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public ProjectParent relation(String relation) {
        this.relation = relation;
        return this;
    }

    /**
     * Get relation
     *
     * @return relation
     **/
    @ApiModelProperty(example = "string", value = "")
    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ProjectParent projectParent = (ProjectParent) o;
        return Objects.equals(this.id, projectParent.id) &&
                Objects.equals(this.fullName, projectParent.fullName) &&
                Objects.equals(this.url, projectParent.url) &&
                Objects.equals(this.path, projectParent.path) &&
                Objects.equals(this.name, projectParent.name) &&
                Objects.equals(this.owner, projectParent.owner) &&
                Objects.equals(this.description, projectParent.description) &&
                Objects.equals(this._private, projectParent._private) &&
                Objects.equals(this.fork, projectParent.fork) &&
                Objects.equals(this.htmlUrl, projectParent.htmlUrl) &&
                Objects.equals(this.forksUrl, projectParent.forksUrl) &&
                Objects.equals(this.keysUrl, projectParent.keysUrl) &&
                Objects.equals(this.collaboratorsUrl, projectParent.collaboratorsUrl) &&
                Objects.equals(this.hooksUrl, projectParent.hooksUrl) &&
                Objects.equals(this.branchesUrl, projectParent.branchesUrl) &&
                Objects.equals(this.tagsUrl, projectParent.tagsUrl) &&
                Objects.equals(this.blobsUrl, projectParent.blobsUrl) &&
                Objects.equals(this.stargazersUrl, projectParent.stargazersUrl) &&
                Objects.equals(this.contributorsUrl, projectParent.contributorsUrl) &&
                Objects.equals(this.commitsUrl, projectParent.commitsUrl) &&
                Objects.equals(this.commentsUrl, projectParent.commentsUrl) &&
                Objects.equals(this.issueCommentUrl, projectParent.issueCommentUrl) &&
                Objects.equals(this.issuesUrl, projectParent.issuesUrl) &&
                Objects.equals(this.pullsUrl, projectParent.pullsUrl) &&
                Objects.equals(this.milestonesUrl, projectParent.milestonesUrl) &&
                Objects.equals(this.notificationsUrl, projectParent.notificationsUrl) &&
                Objects.equals(this.labelsUrl, projectParent.labelsUrl) &&
                Objects.equals(this.releasesUrl, projectParent.releasesUrl) &&
                Objects.equals(this.recommend, projectParent.recommend) &&
                Objects.equals(this.homepage, projectParent.homepage) &&
                Objects.equals(this.language, projectParent.language) &&
                Objects.equals(this.forksCount, projectParent.forksCount) &&
                Objects.equals(this.stargazersCount, projectParent.stargazersCount) &&
                Objects.equals(this.watchersCount, projectParent.watchersCount) &&
                Objects.equals(this.defaultBranch, projectParent.defaultBranch) &&
                Objects.equals(this.openIssuesCount, projectParent.openIssuesCount) &&
                Objects.equals(this.hasIssues, projectParent.hasIssues) &&
                Objects.equals(this.hasWiki, projectParent.hasWiki) &&
                Objects.equals(this.pullRequestsEnabled, projectParent.pullRequestsEnabled) &&
                Objects.equals(this.hasPage, projectParent.hasPage) &&
                Objects.equals(this.license, projectParent.license) &&
                Objects.equals(this.pushedAt, projectParent.pushedAt) &&
                Objects.equals(this.createdAt, projectParent.createdAt) &&
                Objects.equals(this.updatedAt, projectParent.updatedAt) &&
                Objects.equals(this.paas, projectParent.paas) &&
                Objects.equals(this.stared, projectParent.stared) &&
                Objects.equals(this.watched, projectParent.watched) &&
                Objects.equals(this.permission, projectParent.permission) &&
                Objects.equals(this.relation, projectParent.relation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, fullName, url, path, name, owner, description, _private, fork, htmlUrl, forksUrl, keysUrl, collaboratorsUrl, hooksUrl, branchesUrl, tagsUrl, blobsUrl, stargazersUrl, contributorsUrl, commitsUrl, commentsUrl, issueCommentUrl, issuesUrl, pullsUrl, milestonesUrl, notificationsUrl, labelsUrl, releasesUrl, recommend, homepage, language, forksCount, stargazersCount, watchersCount, defaultBranch, openIssuesCount, hasIssues, hasWiki, pullRequestsEnabled, hasPage, license, pushedAt, createdAt, updatedAt, paas, stared, watched, permission, relation);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class ProjectParent {\n");

        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    fullName: ").append(toIndentedString(fullName)).append("\n");
        sb.append("    url: ").append(toIndentedString(url)).append("\n");
        sb.append("    path: ").append(toIndentedString(path)).append("\n");
        sb.append("    name: ").append(toIndentedString(name)).append("\n");
        sb.append("    owner: ").append(toIndentedString(owner)).append("\n");
        sb.append("    description: ").append(toIndentedString(description)).append("\n");
        sb.append("    _private: ").append(toIndentedString(_private)).append("\n");
        sb.append("    fork: ").append(toIndentedString(fork)).append("\n");
        sb.append("    htmlUrl: ").append(toIndentedString(htmlUrl)).append("\n");
        sb.append("    forksUrl: ").append(toIndentedString(forksUrl)).append("\n");
        sb.append("    keysUrl: ").append(toIndentedString(keysUrl)).append("\n");
        sb.append("    collaboratorsUrl: ").append(toIndentedString(collaboratorsUrl)).append("\n");
        sb.append("    hooksUrl: ").append(toIndentedString(hooksUrl)).append("\n");
        sb.append("    branchesUrl: ").append(toIndentedString(branchesUrl)).append("\n");
        sb.append("    tagsUrl: ").append(toIndentedString(tagsUrl)).append("\n");
        sb.append("    blobsUrl: ").append(toIndentedString(blobsUrl)).append("\n");
        sb.append("    stargazersUrl: ").append(toIndentedString(stargazersUrl)).append("\n");
        sb.append("    contributorsUrl: ").append(toIndentedString(contributorsUrl)).append("\n");
        sb.append("    commitsUrl: ").append(toIndentedString(commitsUrl)).append("\n");
        sb.append("    commentsUrl: ").append(toIndentedString(commentsUrl)).append("\n");
        sb.append("    issueCommentUrl: ").append(toIndentedString(issueCommentUrl)).append("\n");
        sb.append("    issuesUrl: ").append(toIndentedString(issuesUrl)).append("\n");
        sb.append("    pullsUrl: ").append(toIndentedString(pullsUrl)).append("\n");
        sb.append("    milestonesUrl: ").append(toIndentedString(milestonesUrl)).append("\n");
        sb.append("    notificationsUrl: ").append(toIndentedString(notificationsUrl)).append("\n");
        sb.append("    labelsUrl: ").append(toIndentedString(labelsUrl)).append("\n");
        sb.append("    releasesUrl: ").append(toIndentedString(releasesUrl)).append("\n");
        sb.append("    recommend: ").append(toIndentedString(recommend)).append("\n");
        sb.append("    homepage: ").append(toIndentedString(homepage)).append("\n");
        sb.append("    language: ").append(toIndentedString(language)).append("\n");
        sb.append("    forksCount: ").append(toIndentedString(forksCount)).append("\n");
        sb.append("    stargazersCount: ").append(toIndentedString(stargazersCount)).append("\n");
        sb.append("    watchersCount: ").append(toIndentedString(watchersCount)).append("\n");
        sb.append("    defaultBranch: ").append(toIndentedString(defaultBranch)).append("\n");
        sb.append("    openIssuesCount: ").append(toIndentedString(openIssuesCount)).append("\n");
        sb.append("    hasIssues: ").append(toIndentedString(hasIssues)).append("\n");
        sb.append("    hasWiki: ").append(toIndentedString(hasWiki)).append("\n");
        sb.append("    pullRequestsEnabled: ").append(toIndentedString(pullRequestsEnabled)).append("\n");
        sb.append("    hasPage: ").append(toIndentedString(hasPage)).append("\n");
        sb.append("    license: ").append(toIndentedString(license)).append("\n");
        sb.append("    pushedAt: ").append(toIndentedString(pushedAt)).append("\n");
        sb.append("    createdAt: ").append(toIndentedString(createdAt)).append("\n");
        sb.append("    updatedAt: ").append(toIndentedString(updatedAt)).append("\n");
        sb.append("    paas: ").append(toIndentedString(paas)).append("\n");
        sb.append("    stared: ").append(toIndentedString(stared)).append("\n");
        sb.append("    watched: ").append(toIndentedString(watched)).append("\n");
        sb.append("    permission: ").append(toIndentedString(permission)).append("\n");
        sb.append("    relation: ").append(toIndentedString(relation)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

