package com.gitee.api.api;

import com.gitee.api.CollectionFormats.*;

import rx.Observable;
import retrofit2.http.*;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;

import com.gitee.api.model.Hook;


public interface WebhooksApi {
  /**
   * 删除一个项目WebHook
   * 删除一个项目WebHook
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param id Webhook的ID (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Void&gt;
   */
  @DELETE("v5/repos/{owner}/{repo}/hooks/{id}")
  Observable<Void> deleteV5ReposOwnerRepoHooksId(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("id") Integer id, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 列出项目的WebHooks
   * 列出项目的WebHooks
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param accessToken 用户授权码 (optional)
   * @param page 当前的页码 (optional, default to 1)
   * @param perPage 每页的数量 (optional, default to 20)
   * @return Call&lt;java.util.List&lt;Hook&gt;&gt;
   */
  @GET("v5/repos/{owner}/{repo}/hooks")
  Observable<java.util.List<Hook>> getV5ReposOwnerRepoHooks(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
  );

  /**
   * 获取项目单个WebHook
   * 获取项目单个WebHook
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param id Webhook的ID (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Hook&gt;
   */
  @GET("v5/repos/{owner}/{repo}/hooks/{id}")
  Observable<Hook> getV5ReposOwnerRepoHooksId(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("id") Integer id, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 更新一个项目WebHook
   * 更新一个项目WebHook
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param id Webhook的ID (required)
   * @param url 远程HTTP URL (required)
   * @param accessToken 用户授权码 (optional)
   * @param password 请求URL时会带上该密码，防止URL被恶意请求 (optional)
   * @param pushEvents Push代码到仓库 (optional, default to true)
   * @param tagPushEvents 提交Tag到仓库 (optional)
   * @param issuesEvents 创建/关闭Issue (optional)
   * @param noteEvents 评论了Issue/代码等等 (optional)
   * @param mergeRequestsEvents 合并请求和合并后 (optional)
   * @return Call&lt;Hook&gt;
   */
  @retrofit2.http.FormUrlEncoded
  @PATCH("v5/repos/{owner}/{repo}/hooks/{id}")
  Observable<Hook> patchV5ReposOwnerRepoHooksId(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("id") Integer id, @retrofit2.http.Field("url") String url, @retrofit2.http.Field("access_token") String accessToken, @retrofit2.http.Field("password") String password, @retrofit2.http.Field("push_events") Boolean pushEvents, @retrofit2.http.Field("tag_push_events") Boolean tagPushEvents, @retrofit2.http.Field("issues_events") Boolean issuesEvents, @retrofit2.http.Field("note_events") Boolean noteEvents, @retrofit2.http.Field("merge_requests_events") Boolean mergeRequestsEvents
  );

  /**
   * 创建一个项目WebHook
   * 创建一个项目WebHook
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param url 远程HTTP URL (required)
   * @param accessToken 用户授权码 (optional)
   * @param password 请求URL时会带上该密码，防止URL被恶意请求 (optional)
   * @param pushEvents Push代码到仓库 (optional, default to true)
   * @param tagPushEvents 提交Tag到仓库 (optional)
   * @param issuesEvents 创建/关闭Issue (optional)
   * @param noteEvents 评论了Issue/代码等等 (optional)
   * @param mergeRequestsEvents 合并请求和合并后 (optional)
   * @return Call&lt;Hook&gt;
   */
  @retrofit2.http.FormUrlEncoded
  @POST("v5/repos/{owner}/{repo}/hooks")
  Observable<Hook> postV5ReposOwnerRepoHooks(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Field("url") String url, @retrofit2.http.Field("access_token") String accessToken, @retrofit2.http.Field("password") String password, @retrofit2.http.Field("push_events") Boolean pushEvents, @retrofit2.http.Field("tag_push_events") Boolean tagPushEvents, @retrofit2.http.Field("issues_events") Boolean issuesEvents, @retrofit2.http.Field("note_events") Boolean noteEvents, @retrofit2.http.Field("merge_requests_events") Boolean mergeRequestsEvents
  );

  /**
   * 测试WebHook是否发送成功
   * 测试WebHook是否发送成功
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param id Webhook的ID (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Void&gt;
   */
  @retrofit2.http.FormUrlEncoded
  @POST("v5/repos/{owner}/{repo}/hooks/{id}/tests")
  Observable<Void> postV5ReposOwnerRepoHooksIdTests(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("id") Integer id, @retrofit2.http.Field("access_token") String accessToken
  );

}
