package com.gitee.api.api;

import com.gitee.api.CollectionFormats.*;

import rx.Observable;
import retrofit2.http.*;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;

import com.gitee.api.model.Event;
import com.gitee.api.model.Project;
import com.gitee.api.model.UserBasic;
import com.gitee.api.model.UserMessage;
import com.gitee.api.model.UserNotification;


public interface ActivityApi {
  /**
   * 取消 star 一个项目
   * 取消 star 一个项目
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Void&gt;
   */
  @DELETE("v5/user/starred/{owner}/{repo}")
  Observable<Void> deleteV5UserStarredOwnerRepo(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 取消 watch 一个项目
   * 取消 watch 一个项目
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Void&gt;
   */
  @DELETE("v5/user/subscriptions/{owner}/{repo}")
  Observable<Void> deleteV5UserSubscriptionsOwnerRepo(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 获取站内所有公开动态
   * 获取站内所有公开动态
   * @param accessToken 用户授权码 (optional)
   * @param page 当前的页码 (optional, default to 1)
   * @param perPage 每页的数量 (optional, default to 20)
   * @return Call&lt;java.util.List&lt;Event&gt;&gt;
   */
  @GET("v5/events")
  Observable<java.util.List<Event>> getV5Events(
    @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
  );

  /**
   * 列出项目的所有公开动态
   * 列出项目的所有公开动态
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param accessToken 用户授权码 (optional)
   * @param page 当前的页码 (optional, default to 1)
   * @param perPage 每页的数量 (optional, default to 20)
   * @return Call&lt;java.util.List&lt;Event&gt;&gt;
   */
  @GET("v5/networks/{owner}/{repo}/events")
  Observable<java.util.List<Event>> getV5NetworksOwnerRepoEvents(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
  );

  /**
   * 列出授权用户的所有私信
   * 列出授权用户的所有私信
   * @param accessToken 用户授权码 (optional)
   * @param unread 是否只显示未读私信，默认：否 (optional)
   * @param since 只显示在给定时间后更新的私信，要求时间格式为 ISO 8601 (optional)
   * @param before 只显示在给定时间前更新的私信，要求时间格式为 ISO 8601 (optional)
   * @param page 当前的页码 (optional, default to 1)
   * @param perPage 每页的数量 (optional, default to 20)
   * @return Call&lt;java.util.List&lt;UserMessage&gt;&gt;
   */
  @GET("v5/notifications/messages")
  Observable<java.util.List<UserMessage>> getV5NotificationsMessages(
    @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("unread") Boolean unread, @retrofit2.http.Query("since") String since, @retrofit2.http.Query("before") String before, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
  );

  /**
   * 获取一个私信
   * 获取一个私信
   * @param id 私信的 ID (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;UserMessage&gt;
   */
  @GET("v5/notifications/messages/{id}")
  Observable<UserMessage> getV5NotificationsMessagesId(
    @retrofit2.http.Path("id") Integer id, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 列出授权用户的所有通知
   * 列出授权用户的所有通知
   * @param accessToken 用户授权码 (optional)
   * @param unread 是否只显示未读消息，默认：否 (optional)
   * @param participating 是否只显示自己直接参与的消息，默认：否 (optional)
   * @param since 只显示在给定时间后更新的消息，要求时间格式为 ISO 8601 (optional)
   * @param before 只显示在给定时间前更新的消息，要求时间格式为 ISO 8601 (optional)
   * @param page 当前的页码 (optional, default to 1)
   * @param perPage 每页的数量 (optional, default to 20)
   * @return Call&lt;java.util.List&lt;UserNotification&gt;&gt;
   */
  @GET("v5/notifications/threads")
  Observable<java.util.List<UserNotification>> getV5NotificationsThreads(
    @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("unread") Boolean unread, @retrofit2.http.Query("participating") Boolean participating, @retrofit2.http.Query("since") String since, @retrofit2.http.Query("before") String before, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
  );

  /**
   * 获取一个通知
   * 获取一个通知
   * @param id 通知的 ID (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;UserNotification&gt;
   */
  @GET("v5/notifications/threads/{id}")
  Observable<UserNotification> getV5NotificationsThreadsId(
    @retrofit2.http.Path("id") Integer id, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 列出组织的公开动态
   * 列出组织的公开动态
   * @param org 组织的路径(path/login) (required)
   * @param accessToken 用户授权码 (optional)
   * @param page 当前的页码 (optional, default to 1)
   * @param perPage 每页的数量 (optional, default to 20)
   * @return Call&lt;java.util.List&lt;Event&gt;&gt;
   */
  @GET("v5/orgs/{org}/events")
  Observable<java.util.List<Event>> getV5OrgsOrgEvents(
    @retrofit2.http.Path("org") String org, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
  );

  /**
   * 列出项目的所有动态
   * 列出项目的所有动态
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param accessToken 用户授权码 (optional)
   * @param page 当前的页码 (optional, default to 1)
   * @param perPage 每页的数量 (optional, default to 20)
   * @return Call&lt;java.util.List&lt;Event&gt;&gt;
   */
  @GET("v5/repos/{owner}/{repo}/events")
  Observable<java.util.List<Event>> getV5ReposOwnerRepoEvents(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
  );

  /**
   * 列出一个项目里的通知
   * 列出一个项目里的通知
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param accessToken 用户授权码 (optional)
   * @param unread 是否只显示未读消息，默认：否 (optional)
   * @param participating 是否只显示自己直接参与的消息，默认：否 (optional)
   * @param since 只显示在给定时间后更新的消息，要求时间格式为 ISO 8601 (optional)
   * @param before 只显示在给定时间前更新的消息，要求时间格式为 ISO 8601 (optional)
   * @param page 当前的页码 (optional, default to 1)
   * @param perPage 每页的数量 (optional, default to 20)
   * @return Call&lt;java.util.List&lt;UserNotification&gt;&gt;
   */
  @GET("v5/repos/{owner}/{repo}/notifications")
  Observable<java.util.List<UserNotification>> getV5ReposOwnerRepoNotifications(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("unread") Boolean unread, @retrofit2.http.Query("participating") Boolean participating, @retrofit2.http.Query("since") String since, @retrofit2.http.Query("before") String before, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
  );

  /**
   * 列出 star 了项目的用户
   * 列出 star 了项目的用户
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param accessToken 用户授权码 (optional)
   * @param page 当前的页码 (optional, default to 1)
   * @param perPage 每页的数量 (optional, default to 20)
   * @return Call&lt;java.util.List&lt;UserBasic&gt;&gt;
   */
  @GET("v5/repos/{owner}/{repo}/stargazers")
  Observable<java.util.List<UserBasic>> getV5ReposOwnerRepoStargazers(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
  );

  /**
   * 列出 watch 了项目的用户
   * 列出 watch 了项目的用户
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param accessToken 用户授权码 (optional)
   * @param page 当前的页码 (optional, default to 1)
   * @param perPage 每页的数量 (optional, default to 20)
   * @return Call&lt;java.util.List&lt;UserBasic&gt;&gt;
   */
  @GET("v5/repos/{owner}/{repo}/subscribers")
  Observable<java.util.List<UserBasic>> getV5ReposOwnerRepoSubscribers(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
  );

  /**
   * 列出授权用户 star 了的项目
   * 列出授权用户 star 了的项目
   * @param accessToken 用户授权码 (optional)
   * @param sort 根据项目创建时间(created)或最后推送时间(updated)进行排序，默认：创建时间 (optional, default to created)
   * @param direction 按递增(asc)或递减(desc)排序，默认：递减 (optional, default to desc)
   * @param page 当前的页码 (optional, default to 1)
   * @param perPage 每页的数量 (optional, default to 20)
   * @return Call&lt;java.util.List&lt;Project&gt;&gt;
   */
  @GET("v5/user/starred")
  Observable<java.util.List<Project>> getV5UserStarred(
    @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("sort") String sort, @retrofit2.http.Query("direction") String direction, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
  );

  /**
   * 检查授权用户是否 star 了一个项目
   * 检查授权用户是否 star 了一个项目
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Void&gt;
   */
  @GET("v5/user/starred/{owner}/{repo}")
  Observable<Void> getV5UserStarredOwnerRepo(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 列出授权用户 watch 了的项目
   * 列出授权用户 watch 了的项目
   * @param accessToken 用户授权码 (optional)
   * @param sort 根据项目创建时间(created)或最后推送时间(updated)进行排序，默认：创建时间 (optional, default to created)
   * @param direction 按递增(asc)或递减(desc)排序，默认：递减 (optional, default to desc)
   * @param page 当前的页码 (optional, default to 1)
   * @param perPage 每页的数量 (optional, default to 20)
   * @return Call&lt;java.util.List&lt;Project&gt;&gt;
   */
  @GET("v5/user/subscriptions")
  Observable<java.util.List<Project>> getV5UserSubscriptions(
    @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("sort") String sort, @retrofit2.http.Query("direction") String direction, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
  );

  /**
   * 检查授权用户是否 watch 了一个项目
   * 检查授权用户是否 watch 了一个项目
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Void&gt;
   */
  @GET("v5/user/subscriptions/{owner}/{repo}")
  Observable<Void> getV5UserSubscriptionsOwnerRepo(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 列出用户的动态
   * 列出用户的动态
   * @param username 用户名(username/login) (required)
   * @param accessToken 用户授权码 (optional)
   * @param page 当前的页码 (optional, default to 1)
   * @param perPage 每页的数量 (optional, default to 20)
   * @return Call&lt;java.util.List&lt;Event&gt;&gt;
   */
  @GET("v5/users/{username}/events")
  Observable<java.util.List<Event>> getV5UsersUsernameEvents(
    @retrofit2.http.Path("username") String username, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
  );

  /**
   * 列出用户所属组织的动态
   * 列出用户所属组织的动态
   * @param username 用户名(username/login) (required)
   * @param org 组织的路径(path/login) (required)
   * @param accessToken 用户授权码 (optional)
   * @param page 当前的页码 (optional, default to 1)
   * @param perPage 每页的数量 (optional, default to 20)
   * @return Call&lt;java.util.List&lt;Event&gt;&gt;
   */
  @GET("v5/users/{username}/events/orgs/{org}")
  Observable<java.util.List<Event>> getV5UsersUsernameEventsOrgsOrg(
    @retrofit2.http.Path("username") String username, @retrofit2.http.Path("org") String org, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
  );

  /**
   * 列出用户的公开动态
   * 列出用户的公开动态
   * @param username 用户名(username/login) (required)
   * @param accessToken 用户授权码 (optional)
   * @param page 当前的页码 (optional, default to 1)
   * @param perPage 每页的数量 (optional, default to 20)
   * @return Call&lt;java.util.List&lt;Event&gt;&gt;
   */
  @GET("v5/users/{username}/events/public")
  Observable<java.util.List<Event>> getV5UsersUsernameEventsPublic(
    @retrofit2.http.Path("username") String username, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
  );

  /**
   * 列出一个用户收到的动态
   * 列出一个用户收到的动态
   * @param username 用户名(username/login) (required)
   * @param accessToken 用户授权码 (optional)
   * @param page 当前的页码 (optional, default to 1)
   * @param perPage 每页的数量 (optional, default to 20)
   * @return Call&lt;java.util.List&lt;Event&gt;&gt;
   */
  @GET("v5/users/{username}/received_events")
  Observable<java.util.List<Event>> getV5UsersUsernameReceivedEvents(
    @retrofit2.http.Path("username") String username, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
  );

  /**
   * 列出一个用户收到的公开动态
   * 列出一个用户收到的公开动态
   * @param username 用户名(username/login) (required)
   * @param accessToken 用户授权码 (optional)
   * @param page 当前的页码 (optional, default to 1)
   * @param perPage 每页的数量 (optional, default to 20)
   * @return Call&lt;java.util.List&lt;Event&gt;&gt;
   */
  @GET("v5/users/{username}/received_events/public")
  Observable<java.util.List<Event>> getV5UsersUsernameReceivedEventsPublic(
    @retrofit2.http.Path("username") String username, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
  );

  /**
   * 列出用户 star 了的项目
   * 列出用户 star 了的项目
   * @param username 用户名(username/login) (required)
   * @param accessToken 用户授权码 (optional)
   * @param page 当前的页码 (optional, default to 1)
   * @param perPage 每页的数量 (optional, default to 20)
   * @param sort 根据项目创建时间(created)或最后推送时间(updated)进行排序，默认：创建时间 (optional, default to created)
   * @param direction 按递增(asc)或递减(desc)排序，默认：递减 (optional, default to desc)
   * @return Call&lt;java.util.List&lt;Project&gt;&gt;
   */
  @GET("v5/users/{username}/starred")
  Observable<java.util.List<Project>> getV5UsersUsernameStarred(
    @retrofit2.http.Path("username") String username, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage, @retrofit2.http.Query("sort") String sort, @retrofit2.http.Query("direction") String direction
  );

  /**
   * 列出用户 watch 了的项目
   * 列出用户 watch 了的项目
   * @param username 用户名(username/login) (required)
   * @param accessToken 用户授权码 (optional)
   * @param page 当前的页码 (optional, default to 1)
   * @param perPage 每页的数量 (optional, default to 20)
   * @param sort 根据项目创建时间(created)或最后推送时间(updated)进行排序，默认：创建时间 (optional, default to created)
   * @param direction 按递增(asc)或递减(desc)排序，默认：递减 (optional, default to desc)
   * @return Call&lt;java.util.List&lt;Project&gt;&gt;
   */
  @GET("v5/users/{username}/subscriptions")
  Observable<java.util.List<Project>> getV5UsersUsernameSubscriptions(
    @retrofit2.http.Path("username") String username, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage, @retrofit2.http.Query("sort") String sort, @retrofit2.http.Query("direction") String direction
  );

  /**
   * 标记一个私信为已读
   * 标记一个私信为已读
   * @param id 私信的 ID (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Void&gt;
   */
  @retrofit2.http.FormUrlEncoded
  @PATCH("v5/notifications/messages/{id}")
  Observable<Void> patchV5NotificationsMessagesId(
    @retrofit2.http.Path("id") Integer id, @retrofit2.http.Field("access_token") String accessToken
  );

  /**
   * 标记一个通知为已读
   * 标记一个通知为已读
   * @param id 通知的 ID (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Void&gt;
   */
  @retrofit2.http.FormUrlEncoded
  @PATCH("v5/notifications/threads/{id}")
  Observable<Void> patchV5NotificationsThreadsId(
    @retrofit2.http.Path("id") Integer id, @retrofit2.http.Field("access_token") String accessToken
  );

  /**
   * 发送私信给指定用户
   * 发送私信给指定用户
   * @param username 用户名(username/login) (required)
   * @param content 私信内容 (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Void&gt;
   */
  @retrofit2.http.FormUrlEncoded
  @POST("v5/notifications/messages")
  Observable<Void> postV5NotificationsMessages(
    @retrofit2.http.Field("username") String username, @retrofit2.http.Field("content") String content, @retrofit2.http.Field("access_token") String accessToken
  );

  /**
   * 标记所有私信为已读
   * 标记所有私信为已读
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Void&gt;
   */
  @retrofit2.http.FormUrlEncoded
  @PUT("v5/notifications/messages")
  Observable<Void> putV5NotificationsMessages(
    @retrofit2.http.Field("access_token") String accessToken
  );

  /**
   * 标记所有通知为已读
   * 标记所有通知为已读
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Void&gt;
   */
  @retrofit2.http.FormUrlEncoded
  @PUT("v5/notifications/threads")
  Observable<Void> putV5NotificationsThreads(
    @retrofit2.http.Field("access_token") String accessToken
  );

  /**
   * 标记一个项目里的通知为已读
   * 标记一个项目里的通知为已读
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Void&gt;
   */
  @retrofit2.http.FormUrlEncoded
  @PUT("v5/repos/{owner}/{repo}/notifications")
  Observable<Void> putV5ReposOwnerRepoNotifications(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Field("access_token") String accessToken
  );

  /**
   * star 一个项目
   * star 一个项目
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Void&gt;
   */
  @retrofit2.http.FormUrlEncoded
  @PUT("v5/user/starred/{owner}/{repo}")
  Observable<Void> putV5UserStarredOwnerRepo(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Field("access_token") String accessToken
  );

  /**
   * watch 一个项目
   * watch 一个项目
   * @param owner 用户名(username/login) (required)
   * @param repo 项目路径(path) (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Void&gt;
   */
  @retrofit2.http.FormUrlEncoded
  @PUT("v5/user/subscriptions/{owner}/{repo}")
  Observable<Void> putV5UserSubscriptionsOwnerRepo(
    @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Field("access_token") String accessToken
  );

}
