package com.gitee.api.api;

import com.gitee.api.model.*;
import retrofit2.http.*;
import rx.Observable;

import java.util.List;


public interface RepositoriesApi {
    /**
     * 删除一个项目
     * 删除一个项目
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param accessToken 用户授权码 (optional)
     * @return Call&lt;Void&gt;
     */
    @DELETE("v5/repos/{owner}/{repo}")
    Observable<Void> deleteV5ReposOwnerRepo(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Query("access_token") String accessToken
    );

    /**
     * 取消保护分支的设置
     * 取消保护分支的设置
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param branch      分支名称 (required)
     * @param accessToken 用户授权码 (optional)
     * @return Call&lt;Void&gt;
     */
    @DELETE("v5/repos/{owner}/{repo}/branches/{branch}/protection")
    Observable<Void> deleteV5ReposOwnerRepoBranchesBranchProtection(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("branch") String branch, @retrofit2.http.Query("access_token") String accessToken
    );

    /**
     * 移除项目成员
     * 移除项目成员
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param username    用户名(username/login) (required)
     * @param accessToken 用户授权码 (optional)
     * @return Call&lt;Void&gt;
     */
    @DELETE("v5/repos/{owner}/{repo}/collaborators/{username}")
    Observable<Void> deleteV5ReposOwnerRepoCollaboratorsUsername(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("username") String username, @retrofit2.http.Query("access_token") String accessToken
    );

    /**
     * 删除Commit评论
     * 删除Commit评论
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param id          评论的ID (required)
     * @param accessToken 用户授权码 (optional)
     * @return Call&lt;Void&gt;
     */
    @DELETE("v5/repos/{owner}/{repo}/comments/{id}")
    Observable<Void> deleteV5ReposOwnerRepoCommentsId(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("id") Integer id, @retrofit2.http.Query("access_token") String accessToken
    );

    /**
     * 删除文件
     * 删除文件
     *
     * @param owner          用户名(username/login) (required)
     * @param repo           项目路径(path) (required)
     * @param path           文件的路径 (required)
     * @param sha            文件被替换的sha值 (required)
     * @param message        提交信息 (required)
     * @param accessToken    用户授权码 (optional)
     * @param branch         分支名称。默认为项目对默认分支 (optional)
     * @param committerName  Committer的名字，默认为当前用户的名字 (optional)
     * @param committerEmail Committer的邮箱，默认为当前用户的邮箱 (optional)
     * @param authorName     Author的名字，默认为当前用户的名字 (optional)
     * @param authorEmail    Author的邮箱，默认为当前用户的邮箱 (optional)
     * @return Call&lt;CommitContent&gt;
     */
    @DELETE("v5/repos/{owner}/{repo}/contents/{path}")
    Observable<CommitContent> deleteV5ReposOwnerRepoContentsPath(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("path") String path, @retrofit2.http.Query("sha") String sha, @retrofit2.http.Query("message") String message, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("branch") String branch, @retrofit2.http.Query("committer[name]") String committerName, @retrofit2.http.Query("committer[email]") String committerEmail, @retrofit2.http.Query("author[name]") String authorName, @retrofit2.http.Query("author[email]") String authorEmail
    );

    /**
     * 删除一个项目公钥
     * 删除一个项目公钥
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param id          公钥 ID (required)
     * @param accessToken 用户授权码 (optional)
     * @return Call&lt;Void&gt;
     */
    @DELETE("v5/repos/{owner}/{repo}/keys/{id}")
    Observable<Void> deleteV5ReposOwnerRepoKeysId(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("id") Integer id, @retrofit2.http.Query("access_token") String accessToken
    );

    /**
     * 删除项目Release
     * 删除项目Release
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param id          (required)
     * @param accessToken 用户授权码 (optional)
     * @return Call&lt;Void&gt;
     */
    @DELETE("v5/repos/{owner}/{repo}/releases/{id}")
    Observable<Void> deleteV5ReposOwnerRepoReleasesId(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("id") Integer id, @retrofit2.http.Query("access_token") String accessToken
    );

    /**
     * 获取一个组织的项目
     * 获取一个组织的项目
     *
     * @param org         组织的路径(path/login) (required)
     * @param accessToken 用户授权码 (optional)
     * @param type        筛选项目的类型，可以是 all, public, private。默认: all (optional, default to all)
     * @param page        当前的页码 (optional, default to 1)
     * @param perPage     每页的数量 (optional, default to 20)
     * @return Call&lt;Project&gt;
     */
    @GET("v5/orgs/{org}/repos")
    Observable<Project> getV5OrgsOrgRepos(
            @retrofit2.http.Path("org") String org, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("type") String type, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
    );

    /**
     * 列出授权用户的某个项目
     * 列出授权用户的某个项目
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param accessToken 用户授权码 (optional)
     * @return Call&lt;Void&gt;
     */
    @GET("v5/repos/{owner}/{repo}")
    Observable<Void> getV5ReposOwnerRepo(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Query("access_token") String accessToken
    );

    /**
     * 获取所有分支
     * 获取所有分支
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param accessToken 用户授权码 (optional)
     * @return Call&lt;java.util.List&lt;Branch&gt;&gt;
     */
    @GET("v5/repos/{owner}/{repo}/branches")
    Observable<java.util.List<Branch>> getV5ReposOwnerRepoBranches(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Query("access_token") String accessToken
    );

    /**
     * 获取单个分支
     * 获取单个分支
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param branch      分支名称 (required)
     * @param accessToken 用户授权码 (optional)
     * @return Call&lt;CompleteBranch&gt;
     */
    @GET("v5/repos/{owner}/{repo}/branches/{branch}")
    Observable<Branch> getV5ReposOwnerRepoBranchesBranch(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("branch") String branch, @retrofit2.http.Query("access_token") String accessToken
    );

    /**
     * 获取项目的所有成员
     * 获取项目的所有成员
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param accessToken 用户授权码 (optional)
     * @return Call&lt;Void&gt;
     */
    @GET("v5/repos/{owner}/{repo}/collaborators")
    Observable<List<BranchUser>> getV5ReposOwnerRepoCollaborators(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Query("access_token") String accessToken
    );

    /**
     * 判断用户是否为项目成员
     * 判断用户是否为项目成员
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param username    用户名(username/login) (required)
     * @param accessToken 用户授权码 (optional)
     * @return Call&lt;Void&gt;
     */
    @GET("v5/repos/{owner}/{repo}/collaborators/{username}")
    Observable<Void> getV5ReposOwnerRepoCollaboratorsUsername(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("username") String username, @retrofit2.http.Query("access_token") String accessToken
    );

    /**
     * 查看项目成员的权限
     * 查看项目成员的权限
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param username    用户名(username/login) (required)
     * @param accessToken 用户授权码 (optional)
     * @return Call&lt;Void&gt;
     */
    @GET("v5/repos/{owner}/{repo}/collaborators/{username}/permission")
    Observable<RepositoriesUserPermission> getV5ReposOwnerRepoCollaboratorsUsernamePermission(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("username") String username, @retrofit2.http.Query("access_token") String accessToken
    );

    /**
     * 获取项目的Commit评论
     * 获取项目的Commit评论
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param accessToken 用户授权码 (optional)
     * @param page        当前的页码 (optional, default to 1)
     * @param perPage     每页的数量 (optional, default to 20)
     * @return Call&lt;Void&gt;
     */
    @GET("v5/repos/{owner}/{repo}/comments")
    Observable<Void> getV5ReposOwnerRepoComments(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
    );

    /**
     * 获取项目的某条Commit评论
     * 获取项目的某条Commit评论
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param id          评论的ID (required)
     * @param accessToken 用户授权码 (optional)
     * @return Call&lt;Void&gt;
     */
    @GET("v5/repos/{owner}/{repo}/comments/{id}")
    Observable<Void> getV5ReposOwnerRepoCommentsId(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("id") Integer id, @retrofit2.http.Query("access_token") String accessToken
    );

    /**
     * 项目的所有提交
     * 项目的所有提交
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param accessToken 用户授权码 (optional)
     * @param sha         提交起始的SHA值或者分支名. 默认: 项目的默认分支 (optional)
     * @param path        包含该文件的提交 (optional)
     * @param author      提交作者的邮箱或个性地址(username/login) (optional)
     * @param since       提交的起始时间，时间格式为 ISO 8601 (optional)
     * @param until       提交的最后时间，时间格式为 ISO 8601 (optional)
     * @param page        当前的页码 (optional, default to 1)
     * @param perPage     每页的数量 (optional, default to 20)
     * @return Call&lt;java.util.List&lt;RepoCommit&gt;&gt;
     */
    @GET("v5/repos/{owner}/{repo}/commits")
    Observable<java.util.List<RepositoriesCommit>> getV5ReposOwnerRepoCommits(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("sha") String sha, @retrofit2.http.Query("path") String path, @retrofit2.http.Query("author") String author, @retrofit2.http.Query("since") String since, @retrofit2.http.Query("until") String until, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
    );

    /**
     * 获取单个Commit的评论
     * 获取单个Commit的评论
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param ref         Commit的Reference (required)
     * @param accessToken 用户授权码 (optional)
     * @param page        当前的页码 (optional, default to 1)
     * @param perPage     每页的数量 (optional, default to 20)
     * @return Call&lt;Void&gt;
     */
    @GET("v5/repos/{owner}/{repo}/commits/{ref}/comments")
    Observable<Void> getV5ReposOwnerRepoCommitsRefComments(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("ref") String ref, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
    );

    /**
     * 项目的某个提交
     * 项目的某个提交
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param sha         提交的SHA值或者分支名 (required)
     * @param accessToken 用户授权码 (optional)
     * @return Call&lt;RepoCommit&gt;
     */
    @GET("v5/repos/{owner}/{repo}/commits/{sha}")
    Observable<RepositoriesCommit> getV5ReposOwnerRepoCommitsSha(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("sha") String sha, @retrofit2.http.Query("access_token") String accessToken
    );

    /**
     * 两个Commits之间对比的版本差异
     * 两个Commits之间对比的版本差异
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param base        Commit提交的SHA值或者分支名作为对比起点 (required)
     * @param head        Commit提交的SHA值或者分支名作为对比终点 (required)
     * @param accessToken 用户授权码 (optional)
     * @return Call&lt;Compare&gt;
     */
    @GET("v5/repos/{owner}/{repo}/compare/{base}...{head}")
    Observable<Compare> getV5ReposOwnerRepoCompareBaseHead(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("base") String base, @retrofit2.http.Path("head") String head, @retrofit2.http.Query("access_token") String accessToken
    );

    /**
     * 获取仓库具体路径下的内容
     * 获取仓库具体路径下的内容
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param path        文件的路径 (required)
     * @param accessToken 用户授权码 (optional)
     * @param ref         分支、tag或commit。默认: 项目的默认分支(通常是master) (optional)
     * @return Call&lt;java.util.List&lt;Content&gt;&gt;
     */
    @GET("v5/repos/{owner}/{repo}/contents(/{path})")
    Observable<java.util.List<Content>> getV5ReposOwnerRepoContentsPath(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("path") String path, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("ref") String ref
    );

    /**
     * 获取项目贡献者
     * 获取项目贡献者
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param accessToken 用户授权码 (optional)
     * @return Call&lt;Void&gt;
     */
    @GET("v5/repos/{owner}/{repo}/contributors")
    Observable<List<RepositoriesContribution>> getV5ReposOwnerRepoContributors(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Query("access_token") String accessToken
    );

    /**
     * 查看项目的Forks
     * 查看项目的Forks
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param accessToken 用户授权码 (optional)
     * @param sort        排序方式: fork的时间(newest, oldest)，star的人数(stargazers) (optional, default to newest)
     * @param page        当前的页码 (optional, default to 1)
     * @param perPage     每页的数量 (optional, default to 20)
     * @return Call&lt;Void&gt;
     */
    @GET("v5/repos/{owner}/{repo}/forks")
    Observable<List<RepositoriesForkUser>> getV5ReposOwnerRepoForks(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("sort") String sort, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
    );

    /**
     * 展示项目的公钥
     * 展示项目的公钥
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param accessToken 用户授权码 (optional)
     * @param page        当前的页码 (optional, default to 1)
     * @param perPage     每页的数量 (optional, default to 20)
     * @return Call&lt;java.util.List&lt;SSHKey&gt;&gt;
     */
    @GET("v5/repos/{owner}/{repo}/keys")
    Observable<java.util.List<SSHKey>> getV5ReposOwnerRepoKeys(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
    );

    /**
     * 获取项目的单个公钥
     * 获取项目的单个公钥
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param id          公钥 ID (required)
     * @param accessToken 用户授权码 (optional)
     * @return Call&lt;SSHKey&gt;
     */
    @GET("v5/repos/{owner}/{repo}/keys/{id}")
    Observable<SSHKey> getV5ReposOwnerRepoKeysId(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("id") Integer id, @retrofit2.http.Query("access_token") String accessToken
    );

    /**
     * 获取Pages信息
     * 获取Pages信息
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param accessToken 用户授权码 (optional)
     * @return Call&lt;Void&gt;
     */
    @GET("v5/repos/{owner}/{repo}/pages")
    Observable<Void> getV5ReposOwnerRepoPages(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Query("access_token") String accessToken
    );

    /**
     * 获取仓库README
     * 获取仓库README
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param accessToken 用户授权码 (optional)
     * @param ref         分支、tag或commit。默认: 项目的默认分支(通常是master) (optional)
     * @return Call&lt;Content&gt;
     */
    @GET("v5/repos/{owner}/{repo}/readme")
    Observable<Content> getV5ReposOwnerRepoReadme(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("ref") String ref
    );

    /**
     * 获取项目的所有Releases
     * 获取项目的所有Releases
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param accessToken 用户授权码 (optional)
     * @param page        当前的页码 (optional, default to 1)
     * @param perPage     每页的数量 (optional, default to 20)
     * @return Call&lt;java.util.List&lt;Release&gt;&gt;
     */
    @GET("v5/repos/{owner}/{repo}/releases")
    Observable<java.util.List<Release>> getV5ReposOwnerRepoReleases(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
    );

    /**
     * 获取项目的单个Releases
     * 获取项目的单个Releases
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param id          发行版本的ID (required)
     * @param accessToken 用户授权码 (optional)
     * @return Call&lt;Release&gt;
     */
    @GET("v5/repos/{owner}/{repo}/releases/{id}")
    Observable<Release> getV5ReposOwnerRepoReleasesId(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("id") Integer id, @retrofit2.http.Query("access_token") String accessToken
    );

    /**
     * 获取项目的最后更新的Release
     * 获取项目的最后更新的Release
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param accessToken 用户授权码 (optional)
     * @return Call&lt;Release&gt;
     */
    @GET("v5/repos/{owner}/{repo}/releases/latest")
    Observable<Release> getV5ReposOwnerRepoReleasesLatest(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Query("access_token") String accessToken
    );

    /**
     * 根据Tag名称获取项目的Release
     * 根据Tag名称获取项目的Release
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param tag         Tag 名称 (required)
     * @param accessToken 用户授权码 (optional)
     * @return Call&lt;Release&gt;
     */
    @GET("v5/repos/{owner}/{repo}/releases/tags/{tag}")
    Observable<Release> getV5ReposOwnerRepoReleasesTagsTag(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("tag") String tag, @retrofit2.http.Query("access_token") String accessToken
    );

    /**
     * 列出项目所有的tags
     * 列出项目所有的tags
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param accessToken 用户授权码 (optional)
     * @return Call&lt;Void&gt;
     */
    @GET("v5/repos/{owner}/{repo}/tags")
    Observable<Void> getV5ReposOwnerRepoTags(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Query("access_token") String accessToken
    );

    /**
     * 列出授权用户的所有项目
     * 列出授权用户的所有项目
     *
     * @param accessToken 用户授权码 (optional)
     * @param visibility  公开(public)、私有(private)或者所有(all)，默认: 所有(all) (optional)
     * @param affiliation owner(授权用户拥有的项目)、collaborator(授权用户为项目成员)、organization_member(授权用户为项目所在组织并有访问项目权限)。                    可以用逗号分隔符组合。如: owner, organization_member 或 owner, collaborator, organization_member (optional)
     * @param type        不能与visibility或affiliation参数一并使用，否则会报422错误 (optional)
     * @param sort        排序方式: 创建时间(created)，更新时间(updated)，最后推送时间(pushed)，项目所属与名称(full_name)。默认: full_name (optional, default to full_name)
     * @param direction   如果sort参数为full_name，用升序(asc)。否则降序(desc) (optional)
     * @param page        当前的页码 (optional, default to 1)
     * @param perPage     每页的数量 (optional, default to 20)
     * @return Call&lt;Void&gt;
     */
    @GET("v5/user/repos")
    Observable<List<Project>> getV5UserRepos(
            @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("visibility") String visibility, @retrofit2.http.Query("affiliation") String affiliation, @retrofit2.http.Query("type") String type, @retrofit2.http.Query("sort") String sort, @retrofit2.http.Query("direction") String direction, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
    );

    /**
     * 获取某个用户的公开项目
     * 获取某个用户的公开项目
     *
     * @param username    用户名(username/login) (required)
     * @param accessToken 用户授权码 (optional)
     * @param type        用户创建的项目(owner)，用户为项目成员(member)，所有(all)。默认: 所有(all) (optional, default to all)
     * @param sort        排序方式: 创建时间(created)，更新时间(updated)，最后推送时间(pushed)，项目所属与名称(full_name)。默认: full_name (optional, default to full_name)
     * @param direction   如果sort参数为full_name，用升序(asc)。否则降序(desc) (optional)
     * @param page        当前的页码 (optional, default to 1)
     * @param perPage     每页的数量 (optional, default to 20)
     * @return Call&lt;Void&gt;
     */
    @GET("v5/users/{username}/repos")
    Observable<Project> getV5UsersUsernameRepos(
            @retrofit2.http.Path("username") String username, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("type") String type, @retrofit2.http.Query("sort") String sort, @retrofit2.http.Query("direction") String direction, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
    );

    /**
     * 更新项目设置
     * 更新项目设置
     *
     * @param owner         用户名(username/login) (required)
     * @param repo          项目路径(path) (required)
     * @param name          项目名称 (required)
     * @param accessToken   用户授权码 (optional)
     * @param description   项目描述 (optional)
     * @param homepage      项目所在地址 (optional)
     * @param hasIssues     允许提Issue与否。默认: 允许(true) (optional, default to true)
     * @param hasWiki       提供Wiki与否。默认: 提供(true) (optional, default to true)
     * @param _private      项目公开或私有。 (optional)
     * @param defaultBranch 更新默认分支 (optional)
     * @return Call&lt;Void&gt;
     */
    @retrofit2.http.FormUrlEncoded
    @PATCH("v5/repos/{owner}/{repo}")
    Observable<Void> patchV5ReposOwnerRepo(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Field("name") String name, @retrofit2.http.Field("access_token") String accessToken, @retrofit2.http.Field("description") String description, @retrofit2.http.Field("homepage") String homepage, @retrofit2.http.Field("has_issues") Boolean hasIssues, @retrofit2.http.Field("has_wiki") Boolean hasWiki, @retrofit2.http.Field("private") Boolean _private, @retrofit2.http.Field("default_branch") String defaultBranch
    );

    /**
     * 更新Commit评论
     * 更新Commit评论
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param id          评论的ID (required)
     * @param body        评论的内容 (required)
     * @param accessToken 用户授权码 (optional)
     * @return Call&lt;Void&gt;
     */
    @retrofit2.http.FormUrlEncoded
    @PATCH("v5/repos/{owner}/{repo}/comments/{id}")
    Observable<Void> patchV5ReposOwnerRepoCommentsId(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("id") Integer id, @retrofit2.http.Field("body") String body, @retrofit2.http.Field("access_token") String accessToken
    );

    /**
     * 更新项目Release
     * 更新项目Release
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param tagName     Tag 名称, 提倡以v字母为前缀做为Release名称，例如v1.0或者v2.3.4 (required)
     * @param name        Release 名称 (required)
     * @param body        Release 描述 (required)
     * @param id          (required)
     * @param accessToken 用户授权码 (optional)
     * @param prerelease  是否为预览版本。默认: false（非预览版本） (optional)
     * @return Call&lt;Release&gt;
     */
    @retrofit2.http.FormUrlEncoded
    @PATCH("v5/repos/{owner}/{repo}/releases/{id}")
    Observable<Release> patchV5ReposOwnerRepoReleasesId(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Field("tag_name") String tagName, @retrofit2.http.Field("name") String name, @retrofit2.http.Field("body") String body, @retrofit2.http.Path("id") Integer id, @retrofit2.http.Field("access_token") String accessToken, @retrofit2.http.Field("prerelease") Boolean prerelease
    );

    /**
     * 创建组织项目
     * 创建组织项目
     *
     * @param name              项目名称 (required)
     * @param org               (required)
     * @param accessToken       用户授权码 (optional)
     * @param description       项目描述 (optional)
     * @param homepage          项目所在地址 (optional)
     * @param hasIssues         允许提Issue与否。默认: 允许(true) (optional, default to true)
     * @param hasWiki           提供Wiki与否。默认: 提供(true) (optional, default to true)
     * @param _private          项目公开或私有。默认: 公开(false) (optional)
     * @param autoInit          值为true时则会用README初始化仓库。默认: 不初始化(false) (optional)
     * @param gitignoreTemplate Git Ingore模版 (optional)
     * @param licenseTemplate   Git Ingore模版 (optional)
     * @return Call&lt;Void&gt;
     */
    @retrofit2.http.FormUrlEncoded
    @POST("v5/orgs/{org}/repos")
    Observable<Void> postV5OrgsOrgRepos(
            @retrofit2.http.Field("name") String name, @retrofit2.http.Path("org") Integer org, @retrofit2.http.Field("access_token") String accessToken, @retrofit2.http.Field("description") String description, @retrofit2.http.Field("homepage") String homepage, @retrofit2.http.Field("has_issues") Boolean hasIssues, @retrofit2.http.Field("has_wiki") Boolean hasWiki, @retrofit2.http.Field("private") Boolean _private, @retrofit2.http.Field("auto_init") Boolean autoInit, @retrofit2.http.Field("gitignore_template") String gitignoreTemplate, @retrofit2.http.Field("license_template") String licenseTemplate
    );

    /**
     * 创建Commit评论
     * 创建Commit评论
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param sha         评论的sha值 (required)
     * @param body        评论的内容 (required)
     * @param accessToken 用户授权码 (optional)
     * @param path        文件的相对路径 (optional)
     * @param position    Diff的相对行数 (optional)
     * @return Call&lt;Void&gt;
     */
    @retrofit2.http.FormUrlEncoded
    @POST("v5/repos/{owner}/{repo}/commits/{sha}/comments")
    Observable<Void> postV5ReposOwnerRepoCommitsShaComments(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("sha") String sha, @retrofit2.http.Field("body") String body, @retrofit2.http.Field("access_token") String accessToken, @retrofit2.http.Field("path") String path, @retrofit2.http.Field("position") Integer position
    );

    /**
     * 新建文件
     * 新建文件
     *
     * @param owner          用户名(username/login) (required)
     * @param repo           项目路径(path) (required)
     * @param path           文件的路径 (required)
     * @param content        文件内容, 要用base64编码 (required)
     * @param message        提交信息 (required)
     * @param accessToken    用户授权码 (optional)
     * @param branch         分支名称。默认为项目对默认分支 (optional)
     * @param committerName  Committer的名字，默认为当前用户的名字 (optional)
     * @param committerEmail Committer的邮箱，默认为当前用户的邮箱 (optional)
     * @param authorName     Author的名字，默认为当前用户的名字 (optional)
     * @param authorEmail    Author的邮箱，默认为当前用户的邮箱 (optional)
     * @return Call&lt;CommitContent&gt;
     */
    @retrofit2.http.FormUrlEncoded
    @POST("v5/repos/{owner}/{repo}/contents/{path}")
    Observable<CommitContent> postV5ReposOwnerRepoContentsPath(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("path") String path, @retrofit2.http.Field("content") String content, @retrofit2.http.Field("message") String message, @retrofit2.http.Field("access_token") String accessToken, @retrofit2.http.Field("branch") String branch, @retrofit2.http.Field("committer[name]") String committerName, @retrofit2.http.Field("committer[email]") String committerEmail, @retrofit2.http.Field("author[name]") String authorName, @retrofit2.http.Field("author[email]") String authorEmail
    );

    /**
     * Fork一个项目
     * Fork一个项目
     *
     * @param owner        用户名(username/login) (required)
     * @param repo         项目路径(path) (required)
     * @param accessToken  用户授权码 (optional)
     * @param organization 组织地址，不填写默认Fork到用户个性地址 (optional)
     * @return Call&lt;Void&gt;
     */
    @retrofit2.http.FormUrlEncoded
    @POST("v5/repos/{owner}/{repo}/forks")
    Observable<Void> postV5ReposOwnerRepoForks(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Field("access_token") String accessToken, @retrofit2.http.Field("organization") String organization
    );

    /**
     * 为项目添加公钥
     * 为项目添加公钥
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param accessToken 用户授权码 (optional)
     * @return Call&lt;SSHKey&gt;
     */
    @retrofit2.http.FormUrlEncoded
    @POST("v5/repos/{owner}/{repo}/keys")
    Observable<SSHKey> postV5ReposOwnerRepoKeys(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Field("access_token") String accessToken, @Field("key") String key
    );

    /**
     * 请求建立Pages
     * 请求建立Pages
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param accessToken 用户授权码 (optional)
     * @return Call&lt;Void&gt;
     */
    @retrofit2.http.FormUrlEncoded
    @POST("v5/repos/{owner}/{repo}/pages/builds")
    Observable<Void> postV5ReposOwnerRepoPagesBuilds(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Field("access_token") String accessToken
    );

    /**
     * 创建项目Release
     * 创建项目Release
     *
     * @param owner           用户名(username/login) (required)
     * @param repo            项目路径(path) (required)
     * @param tagName         Tag 名称, 提倡以v字母为前缀做为Release名称，例如v1.0或者v2.3.4 (required)
     * @param name            Release 名称 (required)
     * @param body            Release 描述 (required)
     * @param targetCommitish 分支名称或者commit SHA, 默认是当前默认分支 (required)
     * @param accessToken     用户授权码 (optional)
     * @param prerelease      是否为预览版本。默认: false（非预览版本） (optional)
     * @return Call&lt;Release&gt;
     */
    @retrofit2.http.FormUrlEncoded
    @POST("v5/repos/{owner}/{repo}/releases")
    Observable<Release> postV5ReposOwnerRepoReleases(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Field("tag_name") String tagName, @retrofit2.http.Field("name") String name, @retrofit2.http.Field("body") String body, @retrofit2.http.Field("target_commitish") String targetCommitish, @retrofit2.http.Field("access_token") String accessToken, @retrofit2.http.Field("prerelease") Boolean prerelease
    );

    /**
     * 创建一个项目
     * 创建一个项目
     *
     * @param name              项目名称 (required)
     * @param accessToken       用户授权码 (optional)
     * @param description       项目描述 (optional)
     * @param homepage          项目所在地址 (optional)
     * @param hasIssues         允许提Issue与否。默认: 允许(true) (optional, default to true)
     * @param hasWiki           提供Wiki与否。默认: 提供(true) (optional, default to true)
     * @param _private          项目公开或私有。默认: 公开(false) (optional)
     * @param autoInit          值为true时则会用README初始化仓库。默认: 不初始化(false) (optional)
     * @param gitignoreTemplate Git Ingore模版 (optional)
     * @param licenseTemplate   License模版 (optional)
     * @return Call&lt;Void&gt;
     */
    @retrofit2.http.FormUrlEncoded
    @POST("v5/user/repos")
    Observable<Void> postV5UserRepos(
            @retrofit2.http.Field("name") String name, @retrofit2.http.Field("access_token") String accessToken, @retrofit2.http.Field("description") String description, @retrofit2.http.Field("homepage") String homepage, @retrofit2.http.Field("has_issues") Boolean hasIssues, @retrofit2.http.Field("has_wiki") Boolean hasWiki, @retrofit2.http.Field("private") Boolean _private, @retrofit2.http.Field("auto_init") Boolean autoInit, @retrofit2.http.Field("gitignore_template") String gitignoreTemplate, @retrofit2.http.Field("license_template") String licenseTemplate
    );

    /**
     * 设置分支保护
     * 设置分支保护
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param branch      分支名称 (required)
     * @param accessToken 用户授权码 (optional)
     * @return Call&lt;CompleteBranch&gt;
     */
    @retrofit2.http.FormUrlEncoded
    @PUT("v5/repos/{owner}/{repo}/branches/{branch}/protection")
    Observable<Void> putV5ReposOwnerRepoBranchesBranchProtection(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("branch") String branch, @retrofit2.http.Field("access_token") String accessToken
    );

    /**
     * 添加项目成员
     * 添加项目成员
     *
     * @param owner       用户名(username/login) (required)
     * @param repo        项目路径(path) (required)
     * @param username    用户名(username/login) (required)
     * @param permission  成员权限: 拉代码(pull)，推代码(push)，管理员(admin)。默认: push (required)
     * @param accessToken 用户授权码 (optional)
     * @return Call&lt;Void&gt;
     */
    @retrofit2.http.FormUrlEncoded
    @PUT("v5/repos/{owner}/{repo}/collaborators/{username}")
    Observable<Void> putV5ReposOwnerRepoCollaboratorsUsername(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("username") String username, @retrofit2.http.Field("permission") String permission, @retrofit2.http.Field("access_token") String accessToken
    );

    /**
     * 更新文件
     * 更新文件
     *
     * @param owner          用户名(username/login) (required)
     * @param repo           项目路径(path) (required)
     * @param path           文件的路径 (required)
     * @param content        文件内容, 要用base64编码 (required)
     * @param sha            文件被替换的sha值 (required)
     * @param message        提交信息 (required)
     * @param accessToken    用户授权码 (optional)
     * @param branch         分支名称。默认为项目对默认分支 (optional)
     * @param committerName  Committer的名字，默认为当前用户的名字 (optional)
     * @param committerEmail Committer的邮箱，默认为当前用户的邮箱 (optional)
     * @param authorName     Author的名字，默认为当前用户的名字 (optional)
     * @param authorEmail    Author的邮箱，默认为当前用户的邮箱 (optional)
     * @return Call&lt;CommitContent&gt;
     */
    @retrofit2.http.FormUrlEncoded
    @PUT("v5/repos/{owner}/{repo}/contents/{path}")
    Observable<Void> putV5ReposOwnerRepoContentsPath(
            @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo, @retrofit2.http.Path("path") String path, @retrofit2.http.Field("content") String content, @retrofit2.http.Field("sha") String sha, @retrofit2.http.Field("message") String message, @retrofit2.http.Field("access_token") String accessToken, @retrofit2.http.Field("branch") String branch, @retrofit2.http.Field("committer[name]") String committerName, @retrofit2.http.Field("committer[email]") String committerEmail, @retrofit2.http.Field("author[name]") String authorName, @retrofit2.http.Field("author[email]") String authorEmail
    );

}
