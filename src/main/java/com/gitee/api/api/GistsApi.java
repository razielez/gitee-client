package com.gitee.api.api;

import com.gitee.api.CollectionFormats.*;

import rx.Observable;
import retrofit2.http.*;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;

import com.gitee.api.model.Code;
import com.gitee.api.model.CodeComment;
import com.gitee.api.model.CodeForks;
import com.gitee.api.model.CodeForksHistory;


public interface GistsApi {
  /**
   * 删除代码片段的评论
   * 删除代码片段的评论
   * @param gistId 代码片段的ID (required)
   * @param id 评论的ID (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Void&gt;
   */
  @DELETE("v5/gists/{gist_id}/comments/{id}")
  Observable<Void> deleteV5GistsGistIdCommentsId(
    @retrofit2.http.Path("gist_id") String gistId, @retrofit2.http.Path("id") Integer id, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 删除该条代码片段
   * 删除该条代码片段
   * @param id 代码片段的ID (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Void&gt;
   */
  @DELETE("v5/gists/{id}")
  Observable<Void> deleteV5GistsId(
    @retrofit2.http.Path("id") String id, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 取消Star代码片段
   * 取消Star代码片段
   * @param id 代码片段的ID (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Void&gt;
   */
  @DELETE("v5/gists/{id}/star")
  Observable<Void> deleteV5GistsIdStar(
    @retrofit2.http.Path("id") String id, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 获取代码片段
   * 获取代码片段
   * @param accessToken 用户授权码 (optional)
   * @param since 起始的更新时间，要求时间格式为 ISO 8601 (optional)
   * @param page 当前的页码 (optional, default to 1)
   * @param perPage 每页的数量 (optional, default to 20)
   * @return Call&lt;java.util.List&lt;Code&gt;&gt;
   */
  @GET("v5/gists")
  Observable<java.util.List<Code>> getV5Gists(
    @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("since") String since, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
  );

  /**
   * 获取代码片段的评论
   * 获取代码片段的评论
   * @param gistId 代码片段的ID (required)
   * @param accessToken 用户授权码 (optional)
   * @param page 当前的页码 (optional, default to 1)
   * @param perPage 每页的数量 (optional, default to 20)
   * @return Call&lt;java.util.List&lt;CodeComment&gt;&gt;
   */
  @GET("v5/gists/{gist_id}/comments")
  Observable<java.util.List<CodeComment>> getV5GistsGistIdComments(
    @retrofit2.http.Path("gist_id") String gistId, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
  );

  /**
   * 获取单条代码片段的评论
   * 获取单条代码片段的评论
   * @param gistId 代码片段的ID (required)
   * @param id 评论的ID (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;CodeComment&gt;
   */
  @GET("v5/gists/{gist_id}/comments/{id}")
  Observable<CodeComment> getV5GistsGistIdCommentsId(
    @retrofit2.http.Path("gist_id") String gistId, @retrofit2.http.Path("id") Integer id, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 获取单条代码片段
   * 获取单条代码片段
   * @param id 代码片段的ID (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;CodeForksHistory&gt;
   */
  @GET("v5/gists/{id}")
  Observable<CodeForksHistory> getV5GistsId(
    @retrofit2.http.Path("id") String id, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 获取代码片段的commit
   * 获取代码片段的commit
   * @param id 代码片段的ID (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;CodeForksHistory&gt;
   */
  @GET("v5/gists/{id}/commits")
  Observable<CodeForksHistory> getV5GistsIdCommits(
    @retrofit2.http.Path("id") String id, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 获取Fork该条代码片段的列表
   * 获取Fork该条代码片段的列表
   * @param id 代码片段的ID (required)
   * @param accessToken 用户授权码 (optional)
   * @param page 当前的页码 (optional, default to 1)
   * @param perPage 每页的数量 (optional, default to 20)
   * @return Call&lt;CodeForks&gt;
   */
  @GET("v5/gists/{id}/forks")
  Observable<CodeForks> getV5GistsIdForks(
    @retrofit2.http.Path("id") String id, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
  );

  /**
   * 判断代码片段是否已Star
   * 判断代码片段是否已Star
   * @param id 代码片段的ID (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Void&gt;
   */
  @GET("v5/gists/{id}/star")
  Observable<Void> getV5GistsIdStar(
    @retrofit2.http.Path("id") String id, @retrofit2.http.Query("access_token") String accessToken
  );

  /**
   * 获取公开的代码片段
   * 获取公开的代码片段
   * @param accessToken 用户授权码 (optional)
   * @param since 起始的更新时间，要求时间格式为 ISO 8601 (optional)
   * @param page 当前的页码 (optional, default to 1)
   * @param perPage 每页的数量 (optional, default to 20)
   * @return Call&lt;java.util.List&lt;Code&gt;&gt;
   */
  @GET("v5/gists/public")
  Observable<java.util.List<Code>> getV5GistsPublic(
    @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("since") String since, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
  );

  /**
   * 获取用户Star的代码片段
   * 获取用户Star的代码片段
   * @param accessToken 用户授权码 (optional)
   * @param since 起始的更新时间，要求时间格式为 ISO 8601 (optional)
   * @param page 当前的页码 (optional, default to 1)
   * @param perPage 每页的数量 (optional, default to 20)
   * @return Call&lt;java.util.List&lt;Code&gt;&gt;
   */
  @GET("v5/gists/starred")
  Observable<java.util.List<Code>> getV5GistsStarred(
    @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("since") String since, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
  );

  /**
   * 获取指定用户的公开代码片段
   * 获取指定用户的公开代码片段
   * @param username 用户名(username/login) (required)
   * @param accessToken 用户授权码 (optional)
   * @param page 当前的页码 (optional, default to 1)
   * @param perPage 每页的数量 (optional, default to 20)
   * @return Call&lt;java.util.List&lt;Code&gt;&gt;
   */
  @GET("v5/users/{username}/gists")
  Observable<java.util.List<Code>> getV5UsersUsernameGists(
    @retrofit2.http.Path("username") String username, @retrofit2.http.Query("access_token") String accessToken, @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("per_page") Integer perPage
  );

  /**
   * 修改代码片段的评论
   * 修改代码片段的评论
   * @param gistId 代码片段的ID (required)
   * @param id 评论的ID (required)
   * @param body 评论内容 (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;CodeComment&gt;
   */
  @retrofit2.http.FormUrlEncoded
  @PATCH("v5/gists/{gist_id}/comments/{id}")
  Observable<CodeComment> patchV5GistsGistIdCommentsId(
    @retrofit2.http.Path("gist_id") String gistId, @retrofit2.http.Path("id") Integer id, @retrofit2.http.Field("body") String body, @retrofit2.http.Field("access_token") String accessToken
  );

  /**
   * 修改代码片段
   * 修改代码片段
   * @param id 代码片段的ID (required)
   * @param files Hash形式的代码片段文件名以及文件内容。如: { \&quot;file1.txt\&quot;: { \&quot;content\&quot;: \&quot;String file contents\&quot; } } (required)
   * @param description 代码片段描述，1~30个字符 (required)
   * @param accessToken 用户授权码 (optional)
   * @param _public 公开/私有，默认: 私有 (optional)
   * @return Call&lt;CodeForksHistory&gt;
   */
  @retrofit2.http.FormUrlEncoded
  @PATCH("v5/gists/{id}")
  Observable<CodeForksHistory> patchV5GistsId(
    @retrofit2.http.Path("id") String id, @retrofit2.http.Field("files") java.util.Map<String, String> files, @retrofit2.http.Field("description") String description, @retrofit2.http.Field("access_token") String accessToken, @retrofit2.http.Field("public") Boolean _public
  );

  /**
   * 创建代码片段
   * 创建代码片段
   * @param files Hash形式的代码片段文件名以及文件内容。如: { \&quot;file1.txt\&quot;: { \&quot;content\&quot;: \&quot;String file contents\&quot; } } (required)
   * @param description 代码片段描述，1~30个字符 (required)
   * @param accessToken 用户授权码 (optional)
   * @param _public 公开/私有，默认: 私有 (optional)
   * @return Call&lt;java.util.List&lt;CodeForksHistory&gt;&gt;
   */
  @retrofit2.http.FormUrlEncoded
  @POST("v5/gists")
  Observable<java.util.List<CodeForksHistory>> postV5Gists(
    @retrofit2.http.Field("files") java.util.Map<String, String> files, @retrofit2.http.Field("description") String description, @retrofit2.http.Field("access_token") String accessToken, @retrofit2.http.Field("public") Boolean _public
  );

  /**
   * 增加代码片段的评论
   * 增加代码片段的评论
   * @param gistId 代码片段的ID (required)
   * @param body 评论内容 (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;CodeComment&gt;
   */
  @retrofit2.http.FormUrlEncoded
  @POST("v5/gists/{gist_id}/comments")
  Observable<CodeComment> postV5GistsGistIdComments(
    @retrofit2.http.Path("gist_id") String gistId, @retrofit2.http.Field("body") String body, @retrofit2.http.Field("access_token") String accessToken
  );

  /**
   * Fork代码片段
   * Fork代码片段
   * @param id 代码片段的ID (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Void&gt;
   */
  @retrofit2.http.FormUrlEncoded
  @POST("v5/gists/{id}/forks")
  Observable<Void> postV5GistsIdForks(
    @retrofit2.http.Path("id") String id, @retrofit2.http.Field("access_token") String accessToken
  );

  /**
   * Star代码片段
   * Star代码片段
   * @param id 代码片段的ID (required)
   * @param accessToken 用户授权码 (optional)
   * @return Call&lt;Void&gt;
   */
  @retrofit2.http.FormUrlEncoded
  @PUT("v5/gists/{id}/star")
  Observable<Void> putV5GistsIdStar(
    @retrofit2.http.Path("id") String id, @retrofit2.http.Field("access_token") String accessToken
  );

}
