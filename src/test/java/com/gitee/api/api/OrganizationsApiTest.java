package com.gitee.api.api;

import com.gitee.api.ApiClient;
import com.gitee.api.model.Group;
import com.gitee.api.model.GroupDetail;
import com.gitee.api.model.GroupMember;
import com.gitee.api.model.UserBasic;
import org.junit.Before;
import org.junit.Test;


/**
 * API tests for OrganizationsApi
 */
public class OrganizationsApiTest {

    private OrganizationsApi api;

    @Before
    public void setup() {
        api = new ApiClient().createService(OrganizationsApi.class);
    }

    /**
     * 移除授权用户所管理组织中的成员
     *
     * 移除授权用户所管理组织中的成员
     */
    @Test
    public void deleteV5OrgsOrgMembershipsUsernameTest() {
        String org = null;
        String username = null;
        String accessToken = null;
        // Void response = api.deleteV5OrgsOrgMembershipsUsername(org, username, accessToken);

        // TODO: test validations
    }
    /**
     * 退出一个组织
     *
     * 退出一个组织
     */
    @Test
    public void deleteV5UserMembershipsOrgsOrgTest() {
        String org = null;
        String accessToken = null;
        // Void response = api.deleteV5UserMembershipsOrgsOrg(org, accessToken);

        // TODO: test validations
    }
    /**
     * 获取一个组织
     *
     * 获取一个组织
     */
    @Test
    public void getV5OrgsOrgTest() {
        String org = null;
        String accessToken = null;
        // Group response = api.getV5OrgsOrg(org, accessToken);

        // TODO: test validations
    }
    /**
     * 列出一个组织的所有成员
     *
     * 列出一个组织的所有成员
     */
    @Test
    public void getV5OrgsOrgMembersTest() {
        String org = null;
        String accessToken = null;
        Integer page = null;
        Integer perPage = null;
        String role = null;
        // java.util.List<UserBasic> response = api.getV5OrgsOrgMembers(org, accessToken, page, perPage, role);

        // TODO: test validations
    }
    /**
     * 获取授权用户所属组织的一个成员
     *
     * 获取授权用户所属组织的一个成员
     */
    @Test
    public void getV5OrgsOrgMembershipsUsernameTest() {
        String org = null;
        String username = null;
        String accessToken = null;
        // GroupMember response = api.getV5OrgsOrgMembershipsUsername(org, username, accessToken);

        // TODO: test validations
    }
    /**
     * 列出授权用户在所属组织的成员资料
     *
     * 列出授权用户在所属组织的成员资料
     */
    @Test
    public void getV5UserMembershipsOrgsTest() {
        String accessToken = null;
        Boolean active = null;
        Integer page = null;
        Integer perPage = null;
        // java.util.List<GroupMember> response = api.getV5UserMembershipsOrgs(accessToken, active, page, perPage);

        // TODO: test validations
    }
    /**
     * 获取授权用户在一个组织的成员资料
     *
     * 获取授权用户在一个组织的成员资料
     */
    @Test
    public void getV5UserMembershipsOrgsOrgTest() {
        String org = null;
        String accessToken = null;
        // GroupMember response = api.getV5UserMembershipsOrgsOrg(org, accessToken);

        // TODO: test validations
    }
    /**
     * 列出授权用户所属的组织
     *
     * 列出授权用户所属的组织
     */
    @Test
    public void getV5UserOrgsTest() {
        String accessToken = null;
        Integer page = null;
        Integer perPage = null;
        Boolean admin = null;
        // java.util.List<Group> response = api.getV5UserOrgs(accessToken, page, perPage, admin);

        // TODO: test validations
    }
    /**
     * 列出用户所属的组织
     *
     * 列出用户所属的组织
     */
    @Test
    public void getV5UsersUsernameOrgsTest() {
        String username = null;
        String accessToken = null;
        Integer page = null;
        Integer perPage = null;
        // java.util.List<Group> response = api.getV5UsersUsernameOrgs(username, accessToken, page, perPage);

        // TODO: test validations
    }
    /**
     * 更新授权用户所管理的组织资料
     *
     * 更新授权用户所管理的组织资料
     */
    @Test
    public void patchV5OrgsOrgTest() {
        String org = null;
        String accessToken = null;
        String email = null;
        String location = null;
        String name = null;
        String description = null;
        String htmlUrl = null;
        // GroupDetail response = api.patchV5OrgsOrg(org, accessToken, email, location, name, description, htmlUrl);

        // TODO: test validations
    }
    /**
     * 更新授权用户在一个组织的成员资料
     *
     * 更新授权用户在一个组织的成员资料
     */
    @Test
    public void patchV5UserMembershipsOrgsOrgTest() {
        String org = null;
        String accessToken = null;
        String remark = null;
        // GroupMember response = api.patchV5UserMembershipsOrgsOrg(org, accessToken, remark);

        // TODO: test validations
    }
    /**
     * 增加或更新授权用户所管理组织的成员
     *
     * 增加或更新授权用户所管理组织的成员
     */
    @Test
    public void putV5OrgsOrgMembershipsUsernameTest() {
        String org = null;
        String username = null;
        String accessToken = null;
        String role = null;
        // GroupMember response = api.putV5OrgsOrgMembershipsUsername(org, username, accessToken, role);

        // TODO: test validations
    }
}
